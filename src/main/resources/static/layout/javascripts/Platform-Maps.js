

function initAutocomplete() {
	
	$('#map').height(window.innerHeight - 96);

	//exemplo
/*  var lat1 = -21.2028539;
    var lng1 = -50.4536792;
    var nomeEvento = '<a href="Platform">nomeEvento1<a>';

    var locations = [
      [nomeEvento, lat1, lng1],
      ['nomeEvento2', -21.2028539, -50.453679],
      ['nomeEvento3', -21.2132497, -50.453757]
    ];
*/
	
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,

      //passar a posição atual da pessoa aqui, é onde o mapa será centralizado
      center: new google.maps.LatLng(-21.2321338, -50.4495253),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });


	//searchbox
    // Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});


    var infowindow = new google.maps.InfoWindow();

    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude,
				acc: position.coords.accuracy
			};
			var dados = pos;
			
			
			//inicio da validação da precisão do local
			if(dados.acc > 2){
				console.log(dados.acc);
				//map.addListener('center_changed', searchAddress());
				var markers = [];
				// [START region_getplaces]
				// Listen for the event fired when the user selects a prediction and retrieve
				// more details for that place.
				searchBox.addListener('places_changed', function() {

					var places = searchBox.getPlaces();

					var result = google.maps.places.PlaceResult;
					console.log(places);
					console.log("To aqui vou chamar o evento");
					$('#botao').trigger('click');
					
					
					

					if (places.length == 0) {
						return;
					}

					// Clear out the old markers.
					markers.forEach(function(marker) {
						marker.setMap(null);
					});
				
					markers = [];

					// For each place, get the icon, name and location.
					var bounds = new google.maps.LatLngBounds();
					places.forEach(function(place) {
						var icon = {
							url: place.icon,
							size: new google.maps.Size(71, 71),
							origin: new google.maps.Point(0, 0),
							anchor: new google.maps.Point(17, 34),
							scaledSize: new google.maps.Size(25, 25)
						};

						// Create a marker for each place.
						markers.push(new google.maps.Marker({
							map: map,
							icon: icon,
							title: place.name,
							position: place.geometry.location
						}));

						if(place.geometry.viewport) {
							
							// Only geocodes have viewport.
							bounds.union(place.geometry.viewport);
						}else {
							bounds.extend(place.geometry.location);
						}
					});
				
					map.fitBounds(bounds);
				});
			}
			//fim

			infowindow.setPosition(pos);
			//infowindow.setContent('Location found.');
			map.setCenter(pos);
			
			
			
			
			console.log("Latitude:" + dados.lat,
						"Longitude:" + dados.lng,
						"Precisão: " + dados.acc);
			
			buscarEventos(dados.lat, dados.lng);
			var marker = new google.maps.Marker({
						  position: pos,
						  map: map,
						  title: "Você está Aqui!"
			});
		})  
	}

	
	function buscarEventos(lati, longi){
		var lat = lati;
		var lon = longi
		var email = document.getElementById('emailUser').value;

		$.ajax({
			type: "POST",
			url: "/BuscarEventoGenero",
			data: {lat : lat,
				   lon : lon,
				   email : email
			},
			success: function(r){
				console.log("Marcando as Festas");
				var locations = [];
				var locatio = [];
				for (var i=0; i<r.length; i++){
					//alert(r[i].nomeEvento);
						
					var lat = r[i].latitude;
					var lng = r[i].longitude;
					var nomeEvento = '<a href="Evento/'+r[i].nomeEvento+'">'+r[i].nomeEvento+'</a>';	
					locations.push([nomeEvento, lat, lng]);
				}
				console.log(locations);
				
				var image = ""; //'img/icone.png';
	
				var marker, i;
				var markers = new Array();
				for (i = 0; i < locations.length; i++) {  
					
					marker = new google.maps.Marker({
						//i[1] = lat i[2] = lng
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),        
						icon: image,
						map: map
					});
					
					markers.push(marker);
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
			},
			error: function(){
				console.log("Erro ao buscar as festas");
			}
		});
	}
}







/*Bucas manual do seu local*/
function searchAddress() {
	
	var addressInput = document.getElementById('pac-input').value;

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({address: addressInput}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
			
			var myResult = results[0].geometry.location;

			var lat = myResult.lat().toString();
			var lng = myResult.lng().toString();
			
			//buscarEventos(lat, lng);
			console.log("Latitude busca manual " + lat);   
			console.log("Longitude buasca manual " + lng);  
		}
	});
}


