
window.onload = function(){
//	buscarEventos(2);
}



function initAutocomplete() {
	
	$('#map').height(window.innerHeight - 320);

	
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,

      //passar a posição atual da pessoa aqui, é onde o mapa será centralizado
      center: new google.maps.LatLng(-21.2321338, -50.4495253),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });


	//searchbox
    // Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});


    var infowindow = new google.maps.InfoWindow();

    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude,
				acc: position.coords.accuracy
			};
			var dados = pos;

			infowindow.setPosition(pos);
			//infowindow.setContent('Location found.');
			map.setCenter(pos);
			
			
			console.log("Latitude: <br>" + dados.lat,
						"Longitude: <br>" + dados.lng,
						"Precisão: " + dados.acc);
			
			buscarEventos();
			var marker = new google.maps.Marker({
						  position: pos,
						  map: map,
						  title: "Você está Aqui!"
			});
			
		})  
	}

	
	function buscarEventos(){
		var idEvento = document.getElementById('idEvent').value;
		$.ajax({
			type: "POST",
			url: "/BuscarEvento",
			data: {idEvento : idEvento},
			success: function(r){
				var locations = [];
				var lat = r.latitude;
				var lng = r.longitude;
				var nomeEvento = '<a href="Events">'+r.nomeEvento+'</a>';	
				locations.push([nomeEvento, lat, lng]);
				
				//passar a posição do evento, é onde o mapa será centralizado
				var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 15,

				  //passar a posição atual da pessoa aqui, é onde o mapa será centralizado
				  center: new google.maps.LatLng(r.latitude, r.longitude),
				  mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var image = ""; //'img/icone.png';
	
				var marker, i;
				var markers = new Array();
				for (i = 0; i < locations.length; i++) {  
					
					marker = new google.maps.Marker({
						//i[1] = lat i[2] = lng
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),        
						icon: image,
						map: map
					});
					
					markers.push(marker);
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
			},
			error: function(){
				console.log("Erro ao buscar as festas");
			}
		});
	}
}