/* INICIANDO PAGE */
window.onload = function(){
  $("#l-branch").addClass('menuSelected');
  document.getElementById("btnSA").value = "adicionar";
  setTimeout(function(){ $("#msgDiv").addClass('noDisplayMsg')}, 6000);
}

/* REMOVENDO O FORM */
function EscondeForm(){

  var idFormFilial = ".formFilalInfo";

  if(!$(idFormFilial).hasClass("noDisplay"))
  {
      $(idFormFilial).addClass("noDisplay");
  }
  else{
      $(idFormFilial).removeClass("noDisplay");
  }
}

/* EDITANDO FILIAL*/
function BuscaFilial(el) {
  var id = el.id;
  $.ajax({
        type: "POST",
        url: "/Branch/Ajax",
        data: {id : id},
        success: function (r)
        {

          document.getElementById("rz").value = r.razaoSocial;
          document.getElementById("nf").value = r.nomeFant;
          document.getElementById("cn").value = r.cnpj;
          document.getElementById("ie").value = r.ie;
          document.getElementById("tel").value = r.telefoneParticular;
          document.getElementById("cap").value = r.capacidade;
          document.getElementById("cep").value = r.cep;
          document.getElementById("cid").value = r.cidade;
          document.getElementById("log").value = r.logradouro;
          document.getElementById("num").value = r.numero;
          document.getElementById("idE").value = r.idEmpresa;
          document.getElementById("idP").value = r.idPessoa;

          SetValue("update");

          $("#btnSA").val('atualizar');
          $("#btnDE").removeClass("noDisplay");
          $("#formFilial").removeClass("noDisplay");

        },
        error: function() {alert("erro");}
  });
}


/*limpa Form da Filal*/
function limpaFormFilial() {
    document.getElementById('rz').value = "";
    document.getElementById('nf').value = "";
    document.getElementById("ie").value = "";
    document.getElementById('cn').value = "";
    document.getElementById('tel').value = "";
    document.getElementById('cep').value = "";
    document.getElementById('cid').value = "";
    document.getElementById('log').value = "";
    document.getElementById('num').value = "";
    document.getElementById('op').value = "save";
    document.getElementById('cap').value = "";
    document.getElementById('idE').value = -1;

    $("#btnDE").addClass('noDisplay');
    $("#btnSA").val('adicionar');
    $("#formFilial").removeClass("noDisplay");
}

function SetValue(v) {
    document.getElementById("op").value = v;
}

/* GOOGLE MAPS */
function initAutocomplete() {
  $("#corpo").height(window.innerHeight - 60);
  $("#map").height(window.innerHeight - 60);

  var map = new google.maps.Map(document.getElementById('map'),
  {
     zoom: 8,
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     center:{lat: -20.9999, lng: -50.9999},
     mapTypeControl: true,
     mapTypeControlOptions: {
         style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
         position: google.maps.ControlPosition.BOTTOM_LEFT
     },
     zoomControl: true,
     zoomControlOptions: {
         position: google.maps.ControlPosition.LEFT_CENTER
     },
     scaleControl: true,
     streetViewControl: true,
     streetViewControlOptions: {
         position: google.maps.ControlPosition.LEFT_TOP
     }
   });

  // Create the search box and link it to the UI element.
  // Cria a search box e liga para o elemento da interface
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


  // Bias the SearchBox results towards current map's viewport.
  // Ajusta o search box ao padrao viewport do mapa
  map.addListener('bounds_changed', function()
  {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function()
  {

    limpaFormFilial();
    SetValue("save");
    var places = searchBox.getPlaces();
    var enderecoFULL = places[0].formatted_address;
    var telefone = places[0].formatted_phone_number;
    var nomeFantasia = places[0].name;
    var enderecoSplited = enderecoFULL.split(",");



      if (enderecoSplited[4]) {
      var logradouroSplited = enderecoSplited[1].split(" - ");
      var cidade = enderecoSplited[2].replace(' ','');
      document.getElementById('cep').value = enderecoSplited[3].replace(' ','');
      document.getElementById('num').value = logradouroSplited[0].replace(' ','');
      document.getElementById('log').value = enderecoSplited[0];
      document.getElementById('tel').value = telefone;
      document.getElementById('nf').value = nomeFantasia;
      document.getElementById('cid').value = cidade;
      document.getElementById('idP').value = $('#idAdm').val();


      searchAddress();
    }
    else{

    }


    if (places.length == 0)
    {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker)
    {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place)
    {
      var icon =
      {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      markers.push(new google.maps.Marker
      ({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport)
      {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else
      {
        bounds.extend(place.geometry.location);
      }
      });
    map.fitBounds(bounds);
  });
  // [END region_getplaces]
}


/*Bucas manual do seu local*/
function searchAddress() {
	
	var addressInput = document.getElementById('pac-input').value;

	var geocoder = new google.maps.Geocoder();

	geocoder.geocode({address: addressInput}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
			
			var myResult = results[0].geometry.location;

            document.getElementById('latitude').value = myResult.lat().toString();
            document.getElementById('longitude').value = myResult.lng().toString();

        }
	});
}


function showQuestion(elment) {

    $("#msgModal").height(window.innerHeight - 60);
    $(elment).removeClass('noDisplay');
    $("#msgModal").removeClass('noDisplay');
}

function hideQuestion(elment) {
    $("#msgModal").addClass('noDisplay');
    $(elment).addClass('noDisplay');
    SetValue("update");

}

//Mask for inputs
 function mascara(t, mask){
      var i = t.value.length;
      var saida = mask.substring(1,0);
      var texto = mask.substring(i)
      if (texto.substring(0,1) != saida){
      t.value += texto.substring(0,1);
        }
      }

//Only Numbers
function tecla(){
    var evt = window.event;
    var tecla = evt.keyCode;

    if(tecla != "48" && tecla != "49" && tecla != "50" && tecla != "51" && tecla != "52" &&
      tecla !="53" && tecla !="54" && tecla !="55" && tecla != "56" && tecla != "57"){
      evt.preventDefault();
    }
  }

  /* Mask for phone */
   function mascaraPhone(o, f) {
       v_obj = o
       v_fun = f
       setTimeout("execmascara()", 1)
  }

   function execmascara() {
       v_obj.value = v_fun(v_obj.value)
  }

   function mtel(v) {
       v = v.replace(/\D/g, ""); //Aqui eu bloqueio digitar algo alem de numeros !
       v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Aqui eu adciono os parenteses dos numeros!
       v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Aqui eu coloco o hífen
       return v;
  }

   function id(el) {
       return document.getElementById(el);
  }
