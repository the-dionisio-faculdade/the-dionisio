/**
 * Created by jonathan on 11/11/16.
 */

window.onload = function(){
	$('#maps').fadeOut("fast");
	$('#vitrineLike').fadeOut("fast");
	$('#vitrine').fadeIn("fast");
	ListarEventoGenero();
	ListarEventoGeneroLike();
} 
 
function selectType(el) {
    var id ="#" + el.id;
    $('.typeSelectedON').removeClass('typeSelectedON');
    $(id).addClass('typeSelectedON');
}


function efectoFadeOutMaps(){
	$('#vitrine').fadeOut(1000);
	$('#vitrineLike').fadeOut(1000);
	$('#maps').removeClass('transparencia');
	$('#maps').fadeIn(3000);
}

function efectoFadeInGenero(){
	$('#maps').fadeOut(1000);
	$('#vitrineLike').fadeOut(1000);
	$('#vitrine').fadeIn(3000);
}

function efectoFadeInLike(){
	$('#maps').fadeOut(1000);
	$('#vitrine').fadeOut(1000);
	$('#vitrineLike').fadeIn(3000);
}

function ListarEventoGenero(){
	var email = document.getElementById('emailUser').value;
		
	$.ajax({
		type: "POST",
		url: "/ListarEventoGenero",
		data: {email : email},
		success: function(r){
			var i = 0;
			for (var i=0; i<r.length; i++){
				var dd = r[i].idEvento;
				
				$('#vitrine').append('<div class="col-md-4 col-sm-4">' +
										'<a href="Evento/'+r[i].nomeEvento+'&'+r[i].idEvento+'"' +
										'onclick="buscarEventos(1)">' +
										'<div class="boxParty" id="party-1"'+
										'style="background: url(layout/images/party3.jpg);' +
										'background-size: cover;' +
										'background-repeat: no-repeat;">'+
										'</div>' +
										'</a>' +
										'<div class="faixaAboutParty">' +
											'<div class="interesse">' +
												'<span id="iNotLikes'+i+'" class="icon-likes" onclick="interesse(this, 0,'+r[i].idEvento+')">' +
													'<i class="fa fa-thumbs-o-down iNotLikes" aria-hidden="true"></i>' +
												'</span>' +
												'<span id="iLikes'+i+'" class="icon-likes" onclick="interesse(this, 1,'+r[i].idEvento+')">' +
													'<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' +											
												'</span>' +
											'</div>'+											
											'<span class="titleParty">'+r[i].nomeEvento+'</span>' +
										'</div>' +										
									 '</div>');						
			}
			
			
		},
		error: function(){
			console.log("Erro ao buscar as festas");
		}
	});
}


function ListarEventoGeneroLike(){
	var email = document.getElementById('emailUser').value;
	$.ajax({
		type: "POST",
		url: "/ListarEventoGeneroLike",
		data: {email : email},
		success: function(r){
			var i = 0;
			for (var i=0; i<r.length; i++){
				
				$('#vitrineLike').append('<div class="col-md-4 col-sm-4">' +
										'<a href="Evento/'+r[i].nomeEvento+'&'+r[i].idEvento+'"' +
										'onclick="buscarEventos()">' +
										'<div class="boxParty" id="party-1"'+
										'style="background: url(layout/images/party3.jpg);' +
										'background-size: cover;' +
										'background-repeat: no-repeat;">'+
										'</div>' +
										'</a>' +
										'<div class="faixaAboutParty">' +
											'<div class="interesse">' +
												'<span id="iNotLikes'+i+'" class="icon-likes" onclick="interesse(this, 0,'+r[i].idEvento+')">' +
													'<i class="fa fa-thumbs-o-down iNotLikes" aria-hidden="true"></i>' +
												'</span>' +
												'<span id="iLikes'+i+'" class="icon-likes" onclick="interesse(this, 1,'+r[i].idEvento+')">' +
													'<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' +											
												'</span>' +
											'</div>'+											
											'<span class="titleParty">'+r[i].nomeEvento+'</span>' +
										'</div>' +										
									 '</div>');						
			}
			
			
		},
		error: function(){
			console.log("Erro ao buscar as festas");
		}
	});
}



function interesse(idSpan, inte,idE, i){
	var id ="#" + idSpan.id;
	var idEvento = idE;
	var interesse = inte;
	var email = document.getElementById('emailUser').value;	
	var data = new Date();
	var dt = data.getDate() + "/" + data.getMonth() + "/" + data.getFullYear();
	if(interesse == 0){
		$(id).removeClass('icon-likes');
		$(id).addClass('notLikes');
		var idd = "#i" + id.substr(5);
		$(idd).addClass('icon-likes');	
	}else{
		$(id).removeClass('icon-likes');
		$(id).addClass('Likes');
		var idd = "#iNot" + id.substr(2);
		$(idd).addClass('icon-likes');
	}
	
	$.ajax({
		type: "POST",
		url: "/Interesse",
		data: {email : email,
			   idEvento : idEvento,
			   interesse : interesse,
			   dt : dt
		},
		success: function(r){
			console.log("Deu certo!")
		},
		error: function(){
			alert("Deu merda");
		}
	});	
}

function searchText(){
	alert("teste");
}

	  
	