window.onload = function() {
    $("#l-events").addClass('menuSelected');

}


/* API WIZARD */
function FadeMap() {
    if ($("#tabMap").hasClass("mapRight")) {
        $("#tabMap").removeClass("mapRight");
        $("#formMapEvent").removeClass('noDisplay');
    } else {
        $("#tabMap").addClass("mapRight");
        $("#formMapEvent").addClass('noDisplay');
    }
}

function RemoveMap() {
    $("#tabMap").addClass("mapRight");
    $("#formMapEvent").addClass('noDisplay');
}

$(document).ready(function() {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function(e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function(e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}

function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

/* TROCA ENTRE CRIAR E EDITAR EVENTOS*/
function showEvent() {
    $("#criarEvento").removeClass("noDisplay");
    $("#listaEvento").addClass("noDisplay");
}

function showEvents() {
    $("#criarEvento").addClass("noDisplay");
    $("#listaEvento").removeClass("noDisplay");
}




/* GOOGLE MAPS */
function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center:{lat: -20.9999, lng: -50.9999},
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            scaleControl: true,
            streetViewControl: true,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        });

    // Create the search box and link it to the UI element.
    // Cria a search box e liga para o elemento da interface
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


    // Bias the SearchBox results towards current map's viewport.
    // Ajusta o search box ao padrao viewport do mapa
    map.addListener('bounds_changed', function()
    {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function()
    {
        var places = searchBox.getPlaces();
        var enderecoFULL = places[0].formatted_address;
        var telefone = places[0].formatted_phone_number;
        var nomeFantasia = places[0].name;
        var enderecoSplited = enderecoFULL.split(",");



        if (enderecoSplited[4]) {
            var logradouroSplited = enderecoSplited[1].split(" - ");
            var cidade = enderecoSplited[2].replace(' ', '');


            $('#cep').val(enderecoSplited[3].replace(' ',''));
            $('#cid').val(cidade);
            $('#tel').val(telefone);
            $('#log').val(enderecoSplited[0]);
            $('#num').val(logradouroSplited[0].replace(' ',''));

        }

        searchAddress();

        if (places.length == 0)
        {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker)
        {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place)
        {
            var icon =
            {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker
            ({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport)
            {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else
            {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
    // [END region_getplaces]
}

/*Bucas manual do seu local*/
function searchAddress() {

    var addressInput = document.getElementById('pac-input').value;

    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({address: addressInput}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            var myResult = results[0].geometry.location;
            alert('getLonLat');
            $('#latitude').val(myResult.lat().toString());
            $('#longitude').val(myResult.lng().toString());
        }
    });
}

/* SDK JAVASCRIPT FACEBOOK PARA BOTÃO DE COMPARTILHAR */
window.fbAsyncInit = function() {
    FB.init({
        appId: '218182378584698',
        cookie: true,
        status: true,
        xfbml: true,
        version: 'v2.6'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/* SKD JAVASCRIPT TWITTER PARA BOTÃO DE COMPARTILHAR */
! function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
        p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = p + '://platform.twitter.com/widgets.js';
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, 'script', 'twitter-wjs');


/* CRUD EVENTO */

var updateEvento = false;

function salvaInfo() {

    var idEvento = $('#idEvento').val();
    var nomeEvento = $('#nomeEvento').val();
    var dataInicialEvento = $('#dataInicialEvento').val();
    var dataFinalEvento = $('#dataFinalEvento').val();
    var descricaoEvento = $('#descricaoEvento').val();
    var idEstabelecimento = $('#filialEvento').val();

    $.ajax({
        type: "POST",
        url: "/SalvaInfo",
        data: {
            idEvento: idEvento,
            nomeEvento: nomeEvento,
            dataInicialEvento: dataInicialEvento,
            dataFinalEvento: dataFinalEvento,
            descricaoEvento: descricaoEvento,
            idEstabelecimento: idEstabelecimento,
            updateEvento:updateEvento

        },
        success: function(r) {
            updateEvento = false;
            if (r != "") {
                $("#idEvento").val(r.idEvento);
                $("#nomeEvento").val(r.nomeEvento);
                $("#dataInicialEvento").val(r.dataInicialEvento);
                $("#dataFinalEvento").val(r.dataFinalEvento);
                $("#descricaoEvento").val(r.descricaoEvento);

            }
        },
        error: function() {
            console.log("erro");
        }
    });
}

var updateLote = false;

function salvaLote() {

    var idLote = $('#idLote').val();
    var idEvento = $("#idEvento").val();
    var dataInicialLote = $('#dataInicialLote').val();
    var dataFinalLote = $('#dataFinalLote').val();
    var descricaoLote = $('#descricaoLote').val();

    $.ajax({
        type: "POST",
        url: "/SalvaLote",
        data: {
            idLote: idLote,
            idEvento: idEvento,
            dataInicialLote: dataInicialLote,
            dataFinalLote: dataFinalLote,
            descricaoLote: descricaoLote,
            updateLote: updateLote
        },
        success: function(lotes) {

            $("#listaLote").empty();
            if (lotes != "") {
                lotes.forEach(function(lote) {
                    addLote(lote);

                });
                updateLote = false;
                limpaCamposStep2();

            }

        },
        error: function() {
            console.log("erro");
        }
    });
}

var estudante = false;
var disticaoSexo = false;
var openBar = false;

function checkBoxSet(el) {
    var id = el.id;
    if (id == "meia") {
        if (estudante) {
            estudante = false;
        } else {
            estudante = true;
        }
    } else if (id == "open") {

        if (openBar) {
            openBar = false;
        } else {
            openBar = true;
        }
    } else if (id == "sexo") {

        if (disticaoSexo) {
            disticaoSexo = false;
        } else {
            disticaoSexo = true;
        }
    }
}

function checkThis(el) {
    var idbox = "#c" + el.id;

    if ($(idbox).hasClass("noDisplay")) {
        $(idbox).removeClass("noDisplay");
        if (idbox == "#csexo") {
            $("#tipoH").removeClass("noDisplay");
            $("#tipoM").removeClass("noDisplay");
            $("#tipoU").addClass("noDisplay");
        }
    } else {
        $(idbox).addClass("noDisplay");
        if (idbox == "#csexo") {
            $("#tipoH").addClass("noDisplay");
            $("#tipoM").addClass("noDisplay");
            $("#tipoU").removeClass("noDisplay");
        }
    }
}

function setCheckFalse() {
    estudante = false;
    disticaoSexo = false;
    openBar = false;
    $("#cmeia").addClass("noDisplay");
    $("#copen").addClass("noDisplay");
    $("#csexo").addClass("noDisplay");
    $("#tipoH").addClass("noDisplay");
    $("#tipoM").addClass("noDisplay");
    $("#tipoU").removeClass("noDisplay");
}

var updateIngresso = false;

function salvaIngresso() {

    var idSetor = $('#idSetor').val();
    var idLote = $('#idLote').val();
    var idIngresso = $('#idIngresso').val();
    var idEvento = $("#idEvento").val();
    var qtdMasculino = $('#qtdMasculino').val();
    var valorMasculino = $('#valorMasculino').val();
    var qtdFeminino = $('#qtdFeminino').val();
    var valorFeminino = $('#valorFeminino').val();
    var qtdUnico = $('#qtdUnico').val();
    var valorUnico = $('#valorUnico').val();

    $.ajax({
        type: "POST",
        url: "/SalvaIngresso",
        data: {
            idLote: idLote,
            idSetor: idSetor,
            idIngresso: idIngresso,
            idEvento: idEvento,
            qtdMasculino: qtdMasculino,
            valorMasculino: valorMasculino,
            qtdFeminino: qtdFeminino,
            valorFeminino: valorFeminino,
            qtdUnico: qtdUnico,
            valorUnico: valorUnico,
            estudante: estudante,
            disticaoSexo: disticaoSexo,
            openBar: openBar,
            updateIngresso: updateIngresso

        },
        success: function(ingressos) {
            setCheckFalse();
            $("#listaIngresso").empty();
            if (ingressos != "") {
                updateIngresso = false;
                ingressos.forEach(function(i) {
                    addIngresso(i);
                    limpaCamposIngresso();
                });
                setCheckFalse();
            }
        },
        error: function() {
            console.log("erro");
        }
    });

}

function deletaLote(el) {
    var idLote = el.id;
    $.ajax({
        type: "POST",
        url: "/DeletaLote",
        data: {
            idLote: idLote
        },
        success: function(r) {
            alert(r.idLote);
            $('#divLote' + r.idLote).remove();

            r.ingressos.forEach(function(i) {
                $('#divIngresso' + i.idIngresso).remove();
            })

        },
        error: function() {
            console.log("erro");
        }
    });
}

function deletaIngresso(el) {
    var idIngresso = el.id;
    $.ajax({
        type: "POST",
        url: "/DeletaIngresso",
        data: {
            idIngresso: idIngresso
        },
        success: function(r) {
            $('#divIngresso' + r).remove();

        },
        error: function() {
            console.log("erro");
        }
    });
}

function deletaEvento(el) {
    var idEvento = el.id;
    $.ajax({
        type: "POST",
        url: "/DeletarEvento",
        data: {
            idEvento: idEvento
        },
        success: function(r) {
           if(r!=""){
            $('#divE' + r).remove();
           }

        },
        error: function() {
            console.log("erro");
        }
    });
}

function buscaEvento(all) {
    var idPessoa = $('#idAdm').val();
    var idEmpresa = $('#filialEventoBusca').val();
    $.ajax({
        type: "POST",
        url: "/BuscaEvento",
        data: {
            idPessoa: idPessoa,
            idEmpresa: idEmpresa,
            all: all
        },
        success: function(eventos) {
            eventos.forEach(function (e) {
               addEvento(e);
            });
        },
        error: function() {
            console.log("erro");
        }
    });

}

function buscaLote(el) {

    var idLote = el.id;
    $.ajax({
        type: "POST",
        url: "/BuscaLote",
        data: {
            idLote: idLote
        },
        success: function(r) {
            updateLote = true;
            $('#idLote').val(r.idLote);
            $("#idEvento").val(r.idEvento);
            $('#descricaoLote').val(r.descricaoLote);
            $('#dataInicialLote').val(r.dataInicialLote);
            $('#dataFinalLote').val(r.dataFinalLote);

        },
        error: function() {
            console.log("erro");
        }
    });

}

function buscaIngresso(el) {
    var idIngresso = el.id;
    $.ajax({
        type: "POST",
        url: "/BuscaIngresso",
        data: {
            idIngresso: idIngresso
        },
        success: function(r) {
            updateIngresso = true;
            setCheckFalse();

            if (r.estudante) {
                document.getElementById('meia').click();
            }
            if (r.openBar) {
                document.getElementById('open').click();
            }
            if (r.disticaoSexo) {
                document.getElementById('sexo').click();
            }

            $('#qtdMasculino').val(r.qtdMasculino);
            $("#valorMasculino").val(r.valorMasculino);
            $('#qtdFeminino').val(r.qtdFeminino);
            $('#valorFeminino').val(r.valorFeminino);
            $('#qtdUnico').val(r.qtdUnico);
            $('#valorUnico').val(r.valorUnico);
            $('#valorUnico').val(r.valorUnico);
            $('#idIngresso').val(r.idIngresso);
            $('#idSetor').val(r.idSetor);


        },
        error: function() {
            console.log("erro");
        }
    });

}

function addLote(lote) {

    $('#idLote').val(lote.idLote);
    $("#listaLote").append('<div  id="divLote' + lote.idLote + '" class="infoLS backColorLote">' +
                           '<span class="txtLS width15">Lote [ ' + lote.descricaoLote + ' ]</span>' +
                           '<span class="txtLS width50">Periodo de venda [ ' + lote.dataInicialLote + ' á ' + lote.dataFinalLote + ' ]</span>' +
                           '<span class="icotLS"><i id="' + lote.idLote + '" onclick="deletaLote(this)" class="fa fa-times"></i></span>' +
                           '<span class="icotLS"><i id="' + lote.idLote + '" onclick="buscaLote(this);editandoLote(this);" class="fa fa-pencil"></i></span> ' +
                           '</div>');
}

function addIngresso(ingresso) {

    var estudante = (ingresso.estudante) ? "sim" : "não";
    var openBar = (ingresso.openBar) ? "sim" : "não";
    var disticaoSexo = (ingresso.disticaoSexo) ? "sim" : "não";

    $("#listaIngresso").append('<div id="divIngresso' + ingresso.idIngresso + '" class="infoLS backColorIngresso">' +
                               '<span class="txtLS width15">Lote [ ' + ingresso.descricaoLote + ' ]</span>' +
                               '<span class="txtLS width15">Setor [ ' + ingresso.descricaoSetor + ' ]</span>' +
                               '<span class="txtLS width15">Estudante [ ' + estudante + ' ]</span>' +
                               '<span class="txtLS width15">OpenBar [ ' + openBar + ' ]</span>' +
                               '<span class="txtLS width15">Distinção [ ' + disticaoSexo + ' ]</span>' +
                               '<span id="' + ingresso.idIngresso + '" onclick="deletaIngresso(this)" class="icotLS"><i class="fa fa-times"></i></span>' +
                               '<span id="' + ingresso.idIngresso + '" onclick="buscaIngresso(this);editandoIngresso(this);" class="icotLS"><i class="fa fa-pencil"></i></span> ' +
                               '</div>');


}

function limpaEventos() {
    $("#listEvent").empty();
}

function addEvento(evento) {
    $("#listEvent").append('<li id="divE"' + evento.idEvento + 'class="eventosLI">' +
                             '<div class="divLiEvento">' +
                                '<span class="txtLS width20">' + evento.nomeEvento + '</span>' +
                                '<span class="txtLS width30">final [ ' + evento.dataInicialEvento + ' ]</span>' +
                                '<span class="txtLS width30">final [ ' + evento.dataFinalEvento + ' ]</span>' +
                                '<span id="'+ evento.idEvento + '" onclick="deletaEvento(this)" class="icotLS"><i class="fa fa-times"></i></span>' +
                                '<span id="' + evento.idEvento + '" onclick="populaLoteIngresso(this);" class="icotLS"><i class="fa fa-pencil"></i></span> ' +
                             '</div>' +
                            '</li>');
}

function editandoLote(el) {

    id = "#divLote" + el.id;
    $(id).addClass('editandoLote');

}

function editandoIngresso(el) {

    id = "#divIngresso" + el.id;
    $(id).addClass('editandoIngresso');
}



function setUpdateEventoOn() {
    updateEvento=true;
}

function populaLoteIngresso(el){

    var idEvento = el.id;

    $.ajax({
        type: "POST",
        url: "/PopulaLoteIngresso",
        data: {
            idEvento: idEvento
        },
        success: function(r) {
            $("#idEvento").val(r[0].idEvento);
            $('#filialEvento').val(r[0].idEstabelecimento);
            $("#nomeEvento").val(r[0].nomeEvento);
            $("#dataInicialEvento").val(r[0].dataInicialEvento);
            $("#dataFinalEvento").val(r[0].dataFinalEvento);
            $("#descricaoEvento").val(r[0].descricaoEvento);

            $("#listaLote").empty();
            $("#listaIngresso").empty();

            r[1].forEach(function (l) {
                addLote(l);
            });

            r[2].forEach(function (i) {
                addIngresso(i);
            });


            showEvent();
            setUpdateEventoOn();
            setEndereco();
            carregaGenerosEvento(r[3]);


        },
        error: function() {
            console.log("erro");
        }
    });
}


function salvaLatLongEvento() {
    var idEvento = $('#idEvento').val();
    var cep = $('#cep').val();
    var cid = $('#cid').val();
    var tel = $('#tel').val();
    var num = $('#num').val();
    var log = $('#log').val();
    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();


    $.ajax({
        type: "POST",
        url: "/SalvaEnderecoEvento",
        data: {
            idEvento: idEvento,
            cep: cep,
            cid: cid,
            num: num,
            log: log,
            latitude: latitude,
            longitude: longitude
        },
        success: function(r) {
            console.log("salvou endereco");

        },
        error: function() {
            console.log("erro");
        }
    });
}

function setEndereco() {
    var idFilial = $('#filialEvento').val();
    $.ajax({
        type: "POST",
        url: "/BuscaEnderecoFilial",
        data: {
            idFilial: idFilial
        },
        success: function(r) {
            $('#cep').val(r.cep);
            $('#cid').val(r.cidade);
            $('#tel').val(r.telefonePublico);
            $('#num').val(r.numero);
            $('#log').val(r.logradouro);
            $('#latitude').val(r.latitude);
            $('#longitude').val(r.longitude);

        },
        error: function() {
            console.log("erro");
        }
    });
}

function atualizaGeneroEvento() {
    var idEvento = $('#idEvento').val();
    var genero = $('#genres').val();

    $.ajax({
        type: "POST",
        url: "/AtualizaGeneroEvento",
        data: {
            idEvento: idEvento,
            genero: genero
        },
        success: function(r) {
            console.log("salvou genro");

        },
        error: function() {
            console.log("erro");
        }
    });
}

function carregaGenerosEvento(generos) {
    generos.forEach(function (g) {
        $('#ckg'+ g.idGenero).removeClass('noDisplay');
        $('#g' + g.idGenero).addClass('boxGeneroSelected');
        genero[g.idGenero - 1]= g.idGenero;
    });
    var generos = genero[0] +','+ genero[1] +','+ genero[2] +','+ genero[3];

    $("#genres").val(generos);
}


function dropZone() {
    document.getElementById('buscaFile').click();
}

function salveFile() {

    document.getElementById('enviaFile').click();
}

/* Validações */
function validaDataEvento() {
    var dtInicial = document.getElementById('dataInicialEvento');
    var dtFinal = document.getElementById('dataFinalEvento');
    if (dataFinalEvento.value < dataInicialEvento.value) {
        alert(" A data final não pode ser menor que data inicial! ");
        $("#dataFinalEvento").val("");
    }
}

function validaDataLoteEvento() {
    var dtFinal = document.getElementById('dataFinalEvento');
    var dtInicialLote = document.getElementById('dataInicialLote');

    if (dataInicialLote.value > dataFinalEvento.value) {
        alert("A data da venda do lote não pode ser maior que o termino do evento !");
          $("#dataInicialLote").val("");
    }
}

function validaDataEventoFinal() {
    var dtFinal = document.getElementById('dataFinalEvento');
    var dtFinalLote = document.getElementById(' dataFinalLote');

    if (dataFinalLote.value > dataFinalEvento.value) {
        alert("A data da venda do lote não pode ser maior que o termino do evento !");
        $("#dataFinalLote").val("");
    }
}

function validaDataAtual() {
    var dtInicial = document.getElementById('dataInicialEvento');
    var date = new Date();

    var dia = date.getDate();
    var mes = date.getMonth() + 1;
    var ano = date.getFullYear();
    var dataFormatada = ano + "-" + mes + "-" + dia;

    if (dataInicialEvento.value < dataFormatada) {
        alert("Impossivel ! a data não pode ser anterior a atual !");
        $("#dataInicialEvento").val("");
    }

}

function validaDataAtualLote() {
    var dtInicialLote = document.getElementById('dataInicialLote');
    var date = new Date();

    var dia = date.getDate();
    var mes = date.getMonth() + 1;
    var ano = date.getFullYear();
    var dataFormatada = ano + "-" + mes + "-" + dia;

    if (dataInicialLote.value < dataFormatada) {
        alert("Impossivel ! a data não pode ser anterior a atual !");
          $("#dataInicialLote").val("");

    }
}

function validaDataAtualLoteFinal(){
  var dtFinalLote = document.getElementById('dataFinalLote');
  var dtInicialLote = document.getElementById('dataInicialLote');

  if(dataFinalLote.value < dataInicialLote.value){
    alert("Impossivel ! a data não pode ser anterior a atual !");
      $("#dataFinalLote").val("");
  }
}

function limpaCampoStep1() {
    document.getElementById("filialEvento").value = "";
    document.getElementById("nomeEvento").value = "";
    document.getElementById("dataInicialEvento").value = "";
    document.getElementById("dataFinalEvento").value = "";
    document.getElementById("descricaoEvento").value = "";
}

function limpaCamposStep2() {
    document.getElementById("descricaoLote").value = "";
    document.getElementById("dataInicialLote").value = "";
    document.getElementById("dataFinalLote").value = "";
}

function limpaCamposIngresso() {
    document.getElementById("idSetor").value = 0;
    document.getElementById("qtdMasculino").value = "";
    document.getElementById("valorMasculino").value = "";
    document.getElementById("qtdFeminino").value = "";
    document.getElementById("valorFeminino").value = "";
    document.getElementById("qtdUnico").value = "";
    document.getElementById("valorUnico").value = "";
}

//mask money
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e) {
    var sep = 0;
    var key = '';
    var i = j = 0;
    var len = len2 = 0;
    var strCheck = '0123456789';
    var aux = aux2 = '';
    var whichCode = (window.Event) ? e.which : e.keyCode;
    if (whichCode == 13) return true;
    key = String.fromCharCode(whichCode); // Valor para o código da Chave
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida
    len = objTextBox.value.length;
    for (i = 0; i < len; i++)
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
    aux = '';
    for (; i < len; i++)
        if (strCheck.indexOf(objTextBox.value.charAt(i)) != -1) aux += objTextBox.value.charAt(i);
    aux += key;
    len = aux.length;
    if (len == 0) objTextBox.value = '';
    if (len == 1) objTextBox.value = '0' + SeparadorDecimal + '0' + aux;
    if (len == 2) objTextBox.value = '0' + SeparadorDecimal + aux;
    if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += SeparadorMilesimo;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        objTextBox.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
            objTextBox.value += aux2.charAt(i);
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
    }
    return false;
}

function tecla() {
    var evt = window.event;
    var tecla = evt.keyCode;

    if (tecla != "48" && tecla != "49" && tecla != "50" && tecla != "51" && tecla != "52" &&
        tecla != "53" && tecla != "54" && tecla != "55" && tecla != "56" && tecla != "57") {
        evt.preventDefault();
    }
}

//validando o evento empty
function validaEvento() {
    var filEvento = document.getElementById('filialEvento');
    var nomEvent = document.getElementById('nomeEvento');
    var dtInicialEvnt = document.getElementById('dataInicialEvento');
    var dtFinalEvnt = document.getElementById('dataFinalEvento');
    var descEvento = document.getElementById('descricaoEvento');

    if (filialEvento.value != "" && nomeEvento.value != "" && dataInicialEvento.value != "" &&
        dataFinalEvento.value != "" && descricaoEvento.value != "") {
        document.getElementById('btnSalvaInfo').click();
    } else {
        alert("TA VAZIOOOOO PORRA!");
    }
}

//valida campos de lote empty
function validaLote() {
    var descLote = document.getElementById('descricaoLote');
    var dtInitLote = document.getElementById('dataInicialLote');
    var dtFinalLote = document.getElementById('dataFinalLote');

    if (descricaoLote.value != "" && dataInicialLote.value != "" && dataFinalLote.value != "") {
        document.getElementById('addLote').click();
    } else {
        alert("TA VAZIOOOOO PORRA!");
    }

}

//validando informações map
function validaMap() {
    var cep = document.getElementById('cep');
    var cid = document.getElementById('cid');
    var num = document.getElementById('num');
    var log = document.getElementById('log');
    var log = document.getElementById('tel');

    if (cep.value != "" && cid.value != "" && num.value != "" &&
        log.value != "" && tel.value != "") {
        document.getElementById('btnMaps').click();
    } else {
        alert("TA VAZIOOOOO PORRA!");
    }
}

var genero = [];
genero[0]=0;
genero[1]=0;
genero[2]=0;
genero[3]=0;

function SelectGenero(el, idG){

var idGenero = "#ck"+el.id;
var idbox = "#"+el.id;

if($(idGenero).hasClass("noDisplay"))
{
$(idGenero).removeClass("noDisplay");
$(idbox).addClass("boxGeneroSelected");
genero[idG]= idG + 1;

}
else
{
$(idGenero).addClass("noDisplay");
$(idbox).removeClass("boxGeneroSelected");
genero[idG]=0;
}

var generos = genero[0] +','+ genero[1] +','+ genero[2] +','+ genero[3];


$("#genres").val(generos);

}

/* Mask for phone */
 function mascaraPhone(o, f) {
     v_obj = o
     v_fun = f
     setTimeout("execmascara()", 1)
}

 function execmascara() {
     v_obj.value = v_fun(v_obj.value)
}

 function mtel(v) {
     v = v.replace(/\D/g, ""); //Aqui eu bloqueio digitar algo alem de numeros !
     v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Aqui eu adciono os parenteses dos numeros!
     v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Aqui eu coloco o hífen
     return v;
}

 function id(el) {
     return document.getElementById(el);
}
