
       /* Config Api */
       var googleUser = {};
       var startApp = function() {
         gapi.load('auth2', function(){
           // Retrieve the singleton for the GoogleAuth library and set up the client.
           auth2 = gapi.auth2.init({
             client_id: '300959686890-dql872743kunhboug77bd9ao4usfbmo3.apps.googleusercontent.com',
             cookiepolicy: 'single_host_origin',
             // Request scopes in addition to 'profile' and 'email'
             scope: 'profile'
           });
           attachSignin(document.getElementById('viaGoogle'));
         });
       };

       /* Resultados Api */
       function attachSignin(element) {
         auth2.attachClickHandler(element, {},
             function(googleUser) {

              var nome = googleUser.getBasicProfile().getName();
              var email = googleUser.getBasicProfile().getEmail();
              var id = googleUser.getBasicProfile().getId();
              var img = googleUser.getBasicProfile().Paa;

              document.getElementById('emailLogin').value = email;
              document.getElementById('passwordLogin').value =id;
              document.getElementById('nameLogin').value = nome;
              document.getElementById('urlImgLogin').value = img;

              document.getElementById('btnLogin').click();

             }, function(error) {
               alert(JSON.stringify(error, undefined, 2));
             });
       }

       window.onload = function(){
         $(".copy_right_text").addClass('textreajust');
       }

       startApp();
