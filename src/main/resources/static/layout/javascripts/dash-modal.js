/* Mask for phone */
function mascaraPhone(o, f) {
  v_obj = o
  v_fun = f
  setTimeout("execmascara()", 1)
}

function execmascara() {
  v_obj.value = v_fun(v_obj.value)
}

function mtel(v) {
  v = v.replace(/\D/g, ""); //Aqui eu bloqueio digitar algo alem de numeros !
  v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Aqui eu adciono os parenteses dos numeros!
  v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Aqui eu coloco o hífen
  return v;
}
