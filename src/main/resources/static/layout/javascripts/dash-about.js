window.onload = function(){
  $("#l-about").addClass('menuSelected');
  setTimeout(function(){ $("#msgDiv").addClass('noDisplayMsg')}, 3000);
}

function abreForm(el){
  var idForm = "#divForm" + el.id;
  var idList = ".lista" + el.id;
  var idBtn = "#" + el.id;
  var idHr = "#hr" + el.id;

  if($(idForm).hasClass("noDisplay"))
  {
      $(idForm).removeClass("noDisplay");
      $(idBtn).removeClass("btn-filial");
      $(idBtn).addClass("btn-normal");
      $(idList).addClass("organizandoLista");
      $(idHr).addClass("hrFilial");
  }
}

function fechaForm(el){
  var idForm = "#divForm" + el.id;
  var idList = ".lista" + el.id;
  var idBtn = "#" + el.id;
  var idTodosForms = ".divForms";
  var idHr = "#hr" + el.id;

  if($(idForm).hasClass("Display"))
  {
      $(idForm).addClass("noDisplay");
      $(idBtn).addClass("btn-filial");
      $(idBtn).removeClass("btn-normal");
      $(idList).removeClass("organizandoLista");
      $(idHr).removeClass("hrFilial");
      removeSimEspecificoAbout();
      removeSimEspecificoRedeSocial();
  }
}

function fechaTodosForms(el){
  var idForm = ".divForms";
  var idList = ".list";
  var idBtn = ".btnForms";
  var idHr = "#hr" + el.id;

  if($(idForm).hasClass("divForms"))
  {
      $(idForm).addClass("noDisplay");
      $(idBtn).addClass("btn-filial");
      $(idBtn).removeClass("btn-normal");
      $(idList).removeClass("organizandoLista");
      $(idHr).removeClass("hrFilial");
      removeSimEspecificoAbout();
      removeSimEspecificoRedeSocial();
  }
}

function LimpandoModal(id){
  var idRedeSocial = ".limpar";
  var idCk = ".limpar";
  var idDivInput = ".limparDiv";
  var idInput = ".inputRedesSociais";
  var idFilial = id.id;
  var idModal = "#myModal" + idFilial;

  $(idModal).on('hide.bs.modal', function (event) {
    $(idRedeSocial).addClass("notDisplay");
    $(idCk).removeClass("boxRedeSocialSelected");
    $(idDivInput).addClass("noDisplay");
    $(idInput).val("");
  });
}

function Selected(el){
  var idCk = "#"+el.id;
  var idRedeSocial = "#ck"+el.id;
  var idInput = "#Div" + el.id;

  if($(idRedeSocial).hasClass("notDisplay")){
      $(idRedeSocial).removeClass("notDisplay");
      $(idCk).addClass("boxRedeSocialSelected");
      $(idInput).removeClass("noDisplay");
  }
  else{
      $(idRedeSocial).addClass("notDisplay");
      $(idCk).removeClass("boxRedeSocialSelected");
      $(idInput).addClass("noDisplay");
  }
}

function irParaOInput(id) {
  var input = "input"+ id;
  document.getElementById(input).focus();
}

/* EDITANDO ABOUT DA FILIAL*/
function BuscaAboutFilial(el) {
    var id = el.id;
    $.ajax({
        type: "POST",
        url: "/About/Ajax",
        data: {id : id},
        success: function (r)
        {
          document.getElementById("telefonePublico" + el.id).value = r.telefonePublico;
          document.getElementById("about" + el.id).value = r.about;
          document.getElementById("idEmpresa" + el.id).value = r.idEmpresa;
          document.getElementById("idPessoa" + el.id).value = r.idPessoa;
          document.getElementById("idRamo" + el.id).value = r.idRamo;
          document.getElementById("nomeFantasia" + el.id).value = r.nomeFant;
          SetValorOperacao("update");
          function SetValorOperacao(operacao) {
          document.getElementById("operacaoFilial" + el.id).value = operacao;
          }
        },
        error: function() {console.log("erro ao carregar as informações de sobre referente a esta filial");}
  });
}

/* AQUI EU ADICIONO O SIM DO FORM ESPECIFICO, PARA QUE ASSIM O UPDATE FUNCIONE PARA AQUELE FORM ESPECIFICO*/
function SimEspecificoAbout(id){
  btnForm = "'btnACTION" + id.id + "'";
  $(".aboutFilial").append($("<span class=\"questionOption removerSimAbout\" onClick=\"document.getElementById(" +btnForm +").click();\">sim</span>"));
}

/* AQUI EU REMOVO O SIM DAQUELE FORM ESPECIFICO PARA QUE QUANDO ELE TROQUE FIQUE APENAS O SIM ESPECIFICO */
function removeSimEspecificoAbout(){
  $(".removerSimAbout").remove();
}

/* AQUI EU ADICIONO O SIM DO FORM ESPECIFICO, PARA QUE ASSIM O UPDATE FUNCIONE PARA AQUELE FORM ESPECIFICO*/
function SimEspecificoRedeSocial(id){
  btnForm = "'btnCRUDREDE" + id.id + "'";
  $(".redeSocial").append($("<span class=\"questionOption removerSimRedeSocial\" onClick=\"document.getElementById(" +btnForm +").click();\">sim</span>"));
}

/* AQUI EU REMOVO O SIM DAQUELE FORM ESPECIFICO PARA QUE QUANDO ELE TROQUE FIQUE APENAS O SIM ESPECIFICO */
function removeSimEspecificoRedeSocial(){
  $(".removerSimRedeSocial").remove();
}

function showQuestion(elment) {
  $("#msgModal").height(window.innerHeight - 60);
  $(elment).removeClass('noDisplay');
  $("#msgModal").removeClass('noDisplay');
}

function hideQuestion(elment) {
  $("#msgModal").addClass('noDisplay');
  $(elment).addClass('noDisplay');
}

/* CARREGAMENTO DOS CAMPOS REDE SOCIAL */
function BuscaRedeSocialFilial(el) {
    var id = el.id;
    id = id.replace('btnCRUDREDE','');

    $.ajax({
        type: "POST",
        url: "/RedeSocialEmpresa/Ajax",
        data: {id : id},
        success: function (r)
        {
          for (var i=0; i<r.length; i++)
          {
            $("#inputDivRedeSocial-"+r[i].idRede+"-"+r[i].idEmpresa).val(r[i].urlComplementar);
            $("#RedeSocial-"+r[i].idRede+"-"+r[i].idEmpresa).removeClass("notDisplay");
            $("#ckRedeSocial-"+r[i].idRede+"-"+r[i].idEmpresa).addClass("boxRedeSocialSelected");
            $("#DivRedeSocial-"+r[i].idRede+"-"+r[i].idEmpresa).removeClass("noDisplay");
          }
          if(r.length != 0)
          {
            $(".btnSAREDE").val("atualizar");
          }
          else
          {
            $(".btnSAREDE").val("adicionar");
          }
        },
        error: function() {console.log("erro");}
  });
}

/* CRUD REDE SOCIAL BY:JONATHAN AND IGOR */
function SalvaRedeSocial(idFilial) {
  var listaUrlComplementar  = [];
  var id = idFilial.id;
  id = id.replace('btnCRUDREDE','');
  listaUrlComplementar.push(id);

  for (var i=1; i<9; i++){
    var urlComplementar = $("#inputDivRedeSocial-"+i+"-"+id).val();
    if (urlComplementar!=""){
        listaUrlComplementar.push(i);
        listaUrlComplementar.push(urlComplementar);
    }
  }

  hideQuestion('#msgUpdateRede');

  $.ajax({
      type: "POST",
      url: "/SalvaRedeSocial",
      data: {listaUrlComplementar:listaUrlComplementar},
      success: function (r){},
      error: function() {console.log("erro");}
  });
}

/* Mask for phone */
 function mascaraPhone(o, f) {
     v_obj = o
     v_fun = f
     setTimeout("execmascara()", 1)
}

 function execmascara() {
     v_obj.value = v_fun(v_obj.value)
}

 function mtel(v) {
     v = v.replace(/\D/g, ""); //Aqui eu bloqueio digitar algo alem de numeros !
     v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Aqui eu adciono os parenteses dos numeros!
     v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Aqui eu coloco o hífen
     return v;
}

 function id(el) {
     return document.getElementById(el);
}
