window.onload = function(){
  $("#l-analitcs").addClass('menuSelected');
  var vergrafico = "true";
}

function gerarGrafico() {
  var idEvento = $('#filialG').val();
  $.ajax({
    type: "POST",
    url: "/BuscaInteresse",
    data: {
      idEvento: idEvento
    },
    success: function(r) {

      console.log(r);
      makeGraph(r[0],r[1],r[2],r[3],r[4]);

    },
    error: function() {
      console.log("erro");
    }
  });

}

function buscaEventoG() {
  var idPessoa = $('#idAdm').val();
  var idEmpresa = $('#filialG').val();
  var all = false;

  $.ajax({
    type: "POST",
    url: "/BuscaEvento",
    data: {
      idPessoa: idPessoa,
      idEmpresa: idEmpresa,
      all: all
    },
    success: function(eventos) {
      $("#eventoG").empty();
      eventos.forEach(function (e) {
        addEventoG(e);
      });

    },
    error: function() {
      console.log("erro");
    }
  });

}


function addEventoG(g) {

  $('#eventoG')
      .append($("<option></option>")
          .attr("value",g.idEvento)
          .text(g.nomeEvento));

}