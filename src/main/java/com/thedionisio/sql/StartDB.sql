ALTER TABLE Empresa ADD CONSTRAINT FOREIGN KEY(idPessoa) REFERENCES Pessoa(idPessoa);
ALTER TABLE Empresa ADD CONSTRAINT FOREIGN KEY(idRamo) REFERENCES Ramo(idRamo);
ALTER TABLE RedeSocialEmpresa ADD CONSTRAINT FOREIGN KEY(idRede) REFERENCES RedeSocial(idRedeSocial);
ALTER TABLE RedeSocialEmpresa ADD CONSTRAINT FOREIGN KEY(idEmpresa) REFERENCES Empresa(idEmpresa);
ALTER TABLE PessoaGenero ADD CONSTRAINT FOREIGN KEY(idPessoa) REFERENCES Pessoa(idPessoa);
ALTER TABLE PessoaGenero ADD CONSTRAINT FOREIGN KEY(idGenero) REFERENCES Genero(idGenero);
ALTER TABLE SubGenero ADD CONSTRAINT FOREIGN KEY(idGenero) REFERENCES Genero(idGenero);
ALTER TABLE EventoGenero ADD CONSTRAINT FOREIGN KEY(idEvento) REFERENCES Evento(idEvento);
ALTER TABLE EventoGenero ADD CONSTRAINT FOREIGN KEY(idGenero) REFERENCES Genero(idGenero);
ALTER TABLE Ingresso ADD CONSTRAINT FOREIGN KEY(idEvento) REFERENCES Evento(idEvento);
ALTER TABLE Ingresso ADD CONSTRAINT FOREIGN KEY(idLote) REFERENCES Lote(idLote);
ALTER TABLE Ingresso ADD CONSTRAINT FOREIGN KEY(idSetor) REFERENCES Setor(idSetor);
ALTER TABLE Evento ADD CONSTRAINT FOREIGN KEY(idEstabelecimento) REFERENCES Empresa(idEmpresa);
ALTER TABLE Interesse ADD CONSTRAINT FOREIGN KEY(idEvento) REFERENCES Evento(idEvento);
ALTER TABLE Interesse ADD CONSTRAINT FOREIGN KEY(idPessoa) REFERENCES Pessoa(idPessoa);
/*INSERT TABLE GENERO*/
INSERT INTO Genero VALUES(1, 'ROCK'), (2, 'PAGODE'), (3, 'ELETRONICA'), (4, 'SERTANEJO');
/*INSERT TABLE SETOR*/
INSERT INTO Setor VALUES(1,'Camarote'), (2,'Pista'), (3,'Vip');
/*INSET TABLE RAMO*/
INSERT INTO Ramo VALUES(1, 'BOATE'), (2, 'CASA DE SHOW'), (3, 'RESTAURANTE'), (4, 'BAR'), (5, 'PROMOTOR DE EVENTOS'), (6, 'AGENCIA DE EVENTO');
/*INSERT TABLE REDESOCIAL*/
INSERT INTO RedeSocial VALUES
  ('1', 'Facebook',     'https://www.facebook.com/'),
  ('2', 'Google +',     'https://plus.google.com/'),
  ('3', 'Twitter',      'https://www.twtter.com/'),
  ('4', 'Site Próprio', 'https://www.'),
  ('5', 'Instagram',    'https://www.instagram.com/'),
  ('6', 'Linkedin',     'https://www.linkedin.com/in/'),
  ('7', 'Youtube',      'https://www.youtube.com/'),
  ('8', 'Snapchat',     '@');




