package com.thedionisio.model.DAO;

import com.thedionisio.model.DTO.LoteDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface LoteDAO extends CrudRepository<LoteDTO, Long>, UpdateDAO{
	
	@Query(value = "select * from Lote l " +
			       "where l.idEvento = :idEvento " +
			       "and l.dataInicialLote = :dataInicialLote " +
			       "and l.dataFinalLote = :dataFinalLote " +
			       "and l.descricaoLote = :descricaoLote",
		  nativeQuery=true)

	LoteDTO findByLote(@Param("idEvento") long idEvento,
					   @Param("dataInicialLote") String dataInicialLote,
					   @Param("dataFinalLote") String dataFinalLote,
					   @Param("descricaoLote") String descricaoLote);

	ArrayList<LoteDTO> findByidEvento (long idEvento);


}
