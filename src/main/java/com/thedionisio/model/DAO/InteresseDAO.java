package com.thedionisio.model.DAO;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.InteresseDTO;

import java.util.ArrayList;

@Repository
public interface InteresseDAO extends CrudRepository<InteresseDTO, Long>{
	
	@Query(value = "select * from Interesse i where i.idPessoa = :idPessoa and i.idEvento =:idEvento",
			nativeQuery = true)
	InteresseDTO findByidInteresse(@Param("idPessoa")long idPessoa,
								   @Param("idEvento")long idEvento);



	@Query(value = "select i.data," +
				   "sum(i.likes) as likes," +
				   "sum(i.notLike) as notLikes," +
				   "i.idEvento" +
				   " from Interesse i where i.idEvento =:idEvento", nativeQuery = true)

	ArrayList<InteresseDTO> findByinEvento(@Param("idEvento")long idEvento);
}
