package com.thedionisio.model.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.GeneroDTO;

@Repository
public interface GeneroDAO extends CrudRepository<GeneroDTO, Long>, UpdateDAO {

}