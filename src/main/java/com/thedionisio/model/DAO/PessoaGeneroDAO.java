
package com.thedionisio.model.DAO;

import com.thedionisio.model.DTO.PessoaGeneroDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Component
@Repository
public interface PessoaGeneroDAO extends CrudRepository<PessoaGeneroDTO, Long>, UpdateDAO {

    ArrayList<PessoaGeneroDTO> findByidPessoa (long idPessoa);

}
