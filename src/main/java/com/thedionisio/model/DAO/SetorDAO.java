package com.thedionisio.model.DAO;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.thedionisio.model.DTO.SetorDTO;

public interface SetorDAO extends CrudRepository<SetorDTO, Long>, UpdateDAO{
	
	@Query(value = "select * from setor s where s.descricaoSetor = :descricaoSetor	order by s.idSetor limit 1", 
			nativeQuery=true)
	SetorDTO findBySetor(@Param("descricaoSetor") String descricaoSetor);
}
