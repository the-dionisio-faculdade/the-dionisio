

package com.thedionisio.model.DAO;


import com.thedionisio.model.DTO.EmpresaDTO;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;



@Repository
public interface EmpresaDAO extends CrudRepository<EmpresaDTO, Long>, UpdateDAO {

	  
	
	  EmpresaDTO findByCnpj(String cnpj);

	  EmpresaDTO findByidEmpresa(long idEmpresa);
	  
	  ArrayList<EmpresaDTO> findByidPessoa(long idPessoa);
	  
	  EmpresaDTO findByNomeFant(String nomeFant);

}


