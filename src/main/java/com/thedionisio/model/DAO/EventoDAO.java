
package com.thedionisio.model.DAO;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.EventoDTO;

import java.util.ArrayList;

@Repository
public interface EventoDAO extends CrudRepository<EventoDTO, Long>, UpdateDAO {
	
		/*Metodo de busca para eventos*/	
		@Query(value = "select * from Evento e " +
					   "where e.nomeEvento = :nomeEvento " +
					   "and e.idEstabelecimento = :idEstabelecimento " +
					   "and e.dataInicialEvento = :dataInicialEvento  " +
					   "and e.dataFinalEvento = :dataFinalEvento",
			   nativeQuery=true)
		EventoDTO findByEvento(@Param("nomeEvento") String nomeEvento, 
							   @Param("idEstabelecimento")	long idEstabelecimento,
							   @Param("dataInicialEvento") String dataInicialEvento,
							   @Param("dataFinalEvento") String dataFinalEvento);
		
		
		
		
		/*Buscar os Eventos conforme o Genero da pessoa*/
		@Query(value = "select 		e.* "+
						"from 		Pessoa p " +
						"inner join PessoaGenero pg on pg.idPessoa = p.idPessoa " +
						"inner join EventoGenero eg on eg.idGenero = pg.idGenero "+
						"inner join Evento e on e.idEvento = eg.idEvento "+
						"inner join Genero g on g.idGenero = pg.idGenero " +
						"where 		e.dataFinalEvento >= curdate() " + 
						"and 		p.idPessoa = :idPessoa order by e.dataInicialEvento ", 
						nativeQuery = true)
		ArrayList<EventoDTO> findByEventoidPessoa(@Param("idPessoa") long idPessoa);
		
		
		/*Metodo de busca os eventos pelo genero da pessoa pela ordem das mais curtidas*/
		@Query(value = 	"select 		e.* " +
					   	"from 			Pessoa p " +
						"inner join 	PessoaGenero pg on pg.idPessoa = p.idPessoa " +
						"inner join 	EventoGenero eg on eg.idGenero = pg.idGenero " +
						"left join  	Evento e on e.idEvento = eg.idEvento " +
						"left join 		Interesse i on i.idEvento = e.idEvento " +
						"where 			p.idPessoa = :idPessoa " +
						"and 			e.dataFinalEvento >= curdate() " +
						"group by 		e.idEvento " +
						"order by 		i.likes desc", 	nativeQuery=true)
		ArrayList<EventoDTO> findByEventoInteresse(@Param("idPessoa") long idPessoa);
		
		
		/*Metodo de busca para os eventos pelo id do estabelecimento*/
		ArrayList<EventoDTO> findByidEstabelecimento(long idEstabelecimento);
		
		/*Metodo de busca de eventos pelo id do mesmo*/
		EventoDTO findByidEvento(long idEvento);
}
