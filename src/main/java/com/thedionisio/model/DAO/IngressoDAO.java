
package com.thedionisio.model.DAO;

import com.thedionisio.model.DTO.EventoDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.IngressoDTO;

import java.util.ArrayList;

@Repository
public interface IngressoDAO extends CrudRepository<IngressoDTO, Long>, UpdateDAO {

    @Query(value = "select * from Ingresso i " +
                   "where i.idLote = :idLote " +
                   "and i.idSetor = :idSetor  " +
                   "and i.idEvento = :idEvento",
          nativeQuery=true)

    IngressoDTO findByIngresso(@Param("idLote")	long idLote,
                               @Param("idSetor")	long idSetor,
                               @Param("idEvento")	long idEvento);

    ArrayList<IngressoDTO> findByidLote(long idLote);

    ArrayList<IngressoDTO> findByidEvento(long idEvento);

}
