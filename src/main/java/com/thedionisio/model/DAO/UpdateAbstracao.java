package com.thedionisio.model.DAO;

import java.lang.reflect.Field;

/**
 * Created by jonathan on 09/11/16.
 */
public class UpdateAbstracao {

    public Object date(Object completo, Object paraAtualizar) throws NoSuchFieldException, IllegalAccessException {

        Class<?> clazz = paraAtualizar.getClass();
        Field[] fields = clazz.getFields();

        for (Field field : fields) {
            Object value = field.get(paraAtualizar);
            if (value != null){
                field.set(completo,value);
            }
        }

        return completo;
    }
}