
package com.thedionisio.model.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.SubGeneroDTO;

@Repository
public interface SubGeneroDAO extends CrudRepository<SubGeneroDTO, Long>, UpdateDAO {

}
