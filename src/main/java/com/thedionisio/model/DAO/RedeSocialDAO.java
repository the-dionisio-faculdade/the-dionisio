
package com.thedionisio.model.DAO;

import com.thedionisio.model.DTO.RedeSocialDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedeSocialDAO extends CrudRepository<RedeSocialDTO, Long>, UpdateDAO {
    
    RedeSocialDTO findByidRedeSocial(long idRedeSocial);
    
    RedeSocialDTO findByDescricaoRede(String descricaoRede);
}
