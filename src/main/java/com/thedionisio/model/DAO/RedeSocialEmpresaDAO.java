package com.thedionisio.model.DAO;

import com.thedionisio.model.DTO.RedeSocialEmpresaDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface RedeSocialEmpresaDAO extends CrudRepository<RedeSocialEmpresaDTO, Long>, UpdateDAO {

  ArrayList<RedeSocialEmpresaDTO> findByidEmpresa(long idEmpresa);
  
  RedeSocialEmpresaDTO findByidRede(long idEmpresa);
}
