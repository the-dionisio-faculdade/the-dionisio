package com.thedionisio.model.DAO;



import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.thedionisio.model.DTO.RamoDTO;

@Repository
public interface RamoDAO extends CrudRepository<RamoDTO, Long>, UpdateDAO{
	
	ArrayList<RamoDTO> findByidRamo(long idRamo);
	
	
	
}
