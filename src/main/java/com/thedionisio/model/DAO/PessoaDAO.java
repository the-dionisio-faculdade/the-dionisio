
package com.thedionisio.model.DAO;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.PessoaDTO;

@Repository
public interface PessoaDAO extends CrudRepository<PessoaDTO, Long>, UpdateDAO {
	
	public PessoaDTO findByEmail(String email);
}
