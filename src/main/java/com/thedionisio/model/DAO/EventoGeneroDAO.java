package com.thedionisio.model.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.thedionisio.model.DTO.EventoGeneroDTO;

import java.util.ArrayList;

@Repository
public interface EventoGeneroDAO extends CrudRepository<EventoGeneroDTO, Long>, UpdateDAO {

    ArrayList<EventoGeneroDTO> findByidEvento (Long idEvento);

}
