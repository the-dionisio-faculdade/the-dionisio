package com.thedionisio.model.DTO;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by jonathan on 21/11/16.
 */
public class FileDTO {

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
