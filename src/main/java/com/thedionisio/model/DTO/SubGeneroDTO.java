package com.thedionisio.model.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="SubGenero")
public class SubGeneroDTO   {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idSubGenero;

	@Column
	public long idGenero;

	@Column(length = 50)
	public String descricaoSubGenero;

	public long getIdSubGenero() {
		return idSubGenero;
	}

	public void setIdSubGenero(long idSubGenero) {
		this.idSubGenero = idSubGenero;
	}

	public long getIdGenero() {
		return idGenero;
	}

	public void setIdGenero(long idGenero) {
		this.idGenero = idGenero;
	}

	public String getDescricaoSubGenero() {
		return descricaoSubGenero;
	}

	public void setDescricaoSubGenero(String descricaoSubGenero) {
		this.descricaoSubGenero = descricaoSubGenero;
	}


}
