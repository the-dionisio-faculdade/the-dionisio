package com.thedionisio.model.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="RedeSocialEmpresa")
public class RedeSocialEmpresaDTO {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idRedeSocialEmpresa;

	@Column
	public long idEmpresa;

	@Column
	public long idRede;

	@Column(length = 255)
	public String urlComplementar;

	public long getIdRedeSocialEmpresa() {
		return idRedeSocialEmpresa;
	}

	public void setIdRedeSocialEmpresa(long idRedeSocialEmpresa) {
		this.idRedeSocialEmpresa = idRedeSocialEmpresa;
	}

	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public long getIdRede() {
		return idRede;
	}

	public void setIdRede(long idRede) {
		this.idRede = idRede;
	}

	public String getUrlComplementar() {
		return urlComplementar;
	}

	public void setUrlComplementar(String urlComplementar) {
		this.urlComplementar = urlComplementar;
	}
}
