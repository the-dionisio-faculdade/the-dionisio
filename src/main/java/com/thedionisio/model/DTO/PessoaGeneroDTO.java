package com.thedionisio.model.DTO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="PessoaGenero")
public class PessoaGeneroDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idPessoaGenero;

	@Column
	public long idPessoa;

	@Column
	public long idGenero;

	@Transient
	public String idsGenero;

	public long getIdPessoaGenero() {
		return idPessoaGenero;
	}

	public void setIdPessoaGenero(long idPessoaGenero) {
		this.idPessoaGenero = idPessoaGenero;
	}

	public long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public long getIdGenero() {
		return idGenero;
	}

	public void setIdGenero(long idGenero) {
		this.idGenero = idGenero;
	}

	public String getIdsGenero() {
		return idsGenero;
	}

	public void setIdsGenero(String idsGenero) {
		this.idsGenero = idsGenero;
	}


}
