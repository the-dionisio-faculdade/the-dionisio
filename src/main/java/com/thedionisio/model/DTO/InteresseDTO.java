package com.thedionisio.model.DTO;



import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="Interesse")
public class InteresseDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idInteresse;

	@Column
	public long idPessoa;

	@Column
	public long idEvento;
	
	@Column
	public boolean likes;
	
	@Column
	public boolean notLike;
	
	@Column(length = 15)
	public String data;
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getIdInteresse() {
		return idInteresse;
	}

	public void setIdInteresse(long idInteresse) {
		this.idInteresse = idInteresse;
	}

	public long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

	public boolean isLikes() {
		return likes;
	}

	public void setLikes(boolean likes) {
		this.likes = likes;
	}

	public boolean isNotLike() {
		return notLike;
	}

	public void setNotLike(boolean notLike) {
		this.notLike = notLike;
	}
	
}
