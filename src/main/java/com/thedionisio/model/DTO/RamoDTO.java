package com.thedionisio.model.DTO;
/**
 * @author 	Diego Dantas
 * @since  	2016-11-06
 * @version 1.0
 * */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="Ramo")
public class RamoDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idRamo;

	@Column(length = 50)
	public String descricaoRamo;

	public long getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(long idRamo) {
		this.idRamo = idRamo;
	}

	public String getDescricaoRamo() {
		return descricaoRamo;
	}

	public void setDescricaoRamo(String descricaoRamo) {
		this.descricaoRamo = descricaoRamo;
	}
}
