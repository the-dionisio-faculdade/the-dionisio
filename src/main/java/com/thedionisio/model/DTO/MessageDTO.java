package com.thedionisio.model.DTO;

/**
 * Created by jonathan on 22/10/16.
 */
public class MessageDTO {


    public String tipo;
    public String texto;
    public String textoAdicional;


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTextoAdicional() {
        return textoAdicional;
    }

    public void setTextoAdicional(String textoAdicional) {
        this.textoAdicional = textoAdicional;
    }



}
