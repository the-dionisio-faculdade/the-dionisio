package com.thedionisio.model.DTO;



import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Table(name="Lote")
public class LoteDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idLote;
	
	@Column
	public long idEvento;
	

	@Column(length = 50)
	public String descricaoLote;

	@Column(length = 15)
	public String dataInicialLote;

	@Column(length = 15)
	public String dataFinalLote;

	@Transient
	public ArrayList<IngressoDTO> ingressos;


	public long getIdLote() {
		return idLote;
	}

	public void setIdLote(long idLote) {
		this.idLote = idLote;
	}

	public long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

	public String getDescricaoLote() {
		return descricaoLote;
	}

	public void setDescricaoLote(String descricaoLote) {
		this.descricaoLote = descricaoLote;
	}

	public String getDataInicialLote() {
		return dataInicialLote;
	}

	public void setDataInicialLote(String dataInicialLote) {
		this.dataInicialLote = dataInicialLote;
	}

	public String getDataFinalLote() {
		return dataFinalLote;
	}

	public void setDataFinalLote(String dataFinalLote) {
		this.dataFinalLote = dataFinalLote;
	}

	public ArrayList<IngressoDTO> getIngressos() {
		return ingressos;
	}

	public void setIngressos(ArrayList<IngressoDTO> ingressos) {
		this.ingressos = ingressos;
	}

}
