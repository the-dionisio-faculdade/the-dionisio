package com.thedionisio.model.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="EventoGenero")
public class EventoGeneroDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idEventoGenero;

	@Column
	public long idGenero;

	@Column
	public long idEvento;

	public long getIdEventoGenero() {
		return idEventoGenero;
	}

	public void setIdEventoGenero(long idEventoGenero) {
		this.idEventoGenero = idEventoGenero;
	}

	public long getIdGenero() {
		return idGenero;
	}

	public void setIdGenero(long idGenero) {
		this.idGenero = idGenero;
	}

	public long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

}
