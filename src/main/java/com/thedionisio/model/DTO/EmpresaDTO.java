package com.thedionisio.model.DTO;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="Empresa")
public class EmpresaDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idEmpresa;

	@Column
	public long idPessoa;

	@Column(length = 50)
	public String razaoSocial;

	@Column(length = 50)
	public String nomeFant;

	@Column(length = 20)
	public String cnpj;

	@Column(length = 20)
	public String ie;



	@Column(length = 20)
	public String telefoneParticular;

	@Column
	public boolean status;

	@Column(length = 255)
	public String about;

	@Column
	public long idRamo;

	@Column(length = 8)
	public String capacidade;

	@Column(length = 20)
	public String numero;

	@Column(length = 20)
	public String telefonePublico;

	@Column(length = 15)
	public String cep;

	@Column(length = 50)
	public String logradouro;

	@Column(length = 50)
	public String cidade;

	@Column(length = 50)
	public String latitude;

	@Column(length = 50)
	public String longitude;

	@Transient
	public String op;
	
	public long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFant() {
		return nomeFant;
	}

	public void setNomeFant(String nomeFant) {
		this.nomeFant = nomeFant;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTelefonePublico() {
		return telefonePublico;
	}

	public void setTelefonePublico(String telefonePublico) {
		this.telefonePublico = telefonePublico;
	}

	public String getTelefoneParticular() {
		return telefoneParticular;
	}

	public void setTelefoneParticular(String telefoneParticular) {
		this.telefoneParticular = telefoneParticular;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public long getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(long idRamo) {
		this.idRamo = idRamo;
	}

	public String getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(String capacidade) {
		this.capacidade = capacidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

}
