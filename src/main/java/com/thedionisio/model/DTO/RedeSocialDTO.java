package com.thedionisio.model.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="RedeSocial")
public class RedeSocialDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idRedeSocial;

	@Column(length = 255)
	public String url;

	@Column(length = 50)
	public String descricaoRede;

	@Transient
	public String op;

	public long getIdRedeSocial() {
		return idRedeSocial;
	}

	public void setIdRedeSocial(long idRedeSocial) {
		this.idRedeSocial = idRedeSocial;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescricaoRede() {
		return descricaoRede;
	}

	public void setDescricaoRede(String descricaoRede) {
		this.descricaoRede = descricaoRede;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}


}
