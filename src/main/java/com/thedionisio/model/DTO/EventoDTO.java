package com.thedionisio.model.DTO;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
@Table(name="Evento")
public class EventoDTO {

	public long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

	public long getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public void setIdEstabelecimento(long idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}

	public String getNomeEvento() {
		return nomeEvento;
	}

	public void setNomeEvento(String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

	public String getDescricaoEvento() {
		return descricaoEvento;
	}

	public void setDescricaoEvento(String descricaoEvento) {
		this.descricaoEvento = descricaoEvento;
	}

	public String getDataInicialEvento() {
		return dataInicialEvento;
	}

	public void setDataInicialEvento(String dataInicialEvento) {
		this.dataInicialEvento = dataInicialEvento;
	}

	public String getDataFinalEvento() {
		return dataFinalEvento;
	}

	public void setDataFinalEvento(String dataFinalEvento) {
		this.dataFinalEvento = dataFinalEvento;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getTelefonePublico() {
		return telefonePublico;
	}

	public void setTelefonePublico(String telefonePublico) {
		this.telefonePublico = telefonePublico;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idEvento;

	@Column
	public long idEstabelecimento;

	@Column(length = 50)
	public String nomeEvento;

	@Column(length = 200)
	public String descricaoEvento;

	@Column(length = 15)
	public String dataInicialEvento;

	@Column(length = 15)
	public String dataFinalEvento;

	@Column(length = 20)
	public String numero;

	@Column(length = 20)
	public String telefonePublico;

	@Column(length = 15)
	public String cep;

	@Column(length = 50)
	public String logradouro;

	@Column(length = 50)
	public String cidade;

	@Column(length = 50)
	public String latitude;

	@Column(length = 50)
	public String longitude;




}
