package com.thedionisio.model.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Service;


@Service
@Entity
@Table(name="Genero")
public  class GeneroDTO {

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idGenero;

	@Column(length = 50)
	public String descricaoGenero;

	public long getidGenero() {
		return idGenero;
	}

	public void setidGenero(long idGenero) {
		this.idGenero = idGenero;
	}

	public String getDescricaoGenero() {
		return descricaoGenero;
	}

	public void setDescricaoGenero(String descricaoGenero) {
		this.descricaoGenero = descricaoGenero;
	}

}
