package com.thedionisio.model.DTO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="Ingresso")
public class IngressoDTO {
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long idIngresso;

	@Column
	public long idLote;

	@Column
	public long idSetor;

	@Column
	public long idEvento;

	@Column(length = 10)
	public String valorMasculino;

	@Column(length = 10)
	public String valorFeminino;

	@Column(length = 10)
	public String valorUnico;

	@Column(length = 10)
	public String qtdMasculino;

	@Column(length = 10)
	public String qtdFeminino;

	@Column(length = 10)
	public String qtdUnico;

	@Column
	public boolean estudante;

	@Column
	public boolean disticaoSexo;

	@Column
	public boolean openBar;

	@Transient
	public String descricaoSetor;

	@Transient
	public String descricaoLote;
	
	public long getIdIngresso() {
		return idIngresso;
	}

	public void setIdIngresso(long idIngresso) {
		this.idIngresso = idIngresso;
	}

	public long getIdLote() {
		return idLote;
	}

	public void setIdLote(long idLote) {
		this.idLote = idLote;
	}

	public long getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(long idSetor) {
		this.idSetor = idSetor;
	}

	public long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(long idEvento) {
		this.idEvento = idEvento;
	}

	public String getValorMasculino() {
		return valorMasculino;
	}

	public void setValorMasculino(String valorMasculino) {
		this.valorMasculino = valorMasculino;
	}

	public String getValorFeminino() {
		return valorFeminino;
	}

	public void setValorFeminino(String valorFeminino) {
		this.valorFeminino = valorFeminino;
	}

	public String getValorUnico() {
		return valorUnico;
	}

	public void setValorUnico(String valorUnico) {
		this.valorUnico = valorUnico;
	}

	public String getQtdMasculino() {
		return qtdMasculino;
	}

	public void setQtdMasculino(String qtdMasculino) {
		this.qtdMasculino = qtdMasculino;
	}

	public String getQtdFeminino() {
		return qtdFeminino;
	}

	public void setQtdFeminino(String qtdFeminino) {
		this.qtdFeminino = qtdFeminino;
	}

	public String getQtdUnico() {
		return qtdUnico;
	}

	public void setQtdUnico(String qtdUnico) {
		this.qtdUnico = qtdUnico;
	}

	public boolean isEstudante() {
		return estudante;
	}

	public void setEstudante(boolean estudante) {
		this.estudante = estudante;
	}

	public boolean isDisticaoSexo() {
		return disticaoSexo;
	}

	public void setDisticaoSexo(boolean disticaoSexo) {
		this.disticaoSexo = disticaoSexo;
	}

	public boolean isOpenBar() {
		return openBar;
	}

	public void setOpenBar(boolean openBar) {
		this.openBar = openBar;
	}

	public String getDescricaoSetor() {
		return descricaoSetor;
	}

	public void setDescricaoSetor(String descricaoSetor) {
		this.descricaoSetor = descricaoSetor;
	}

	public String getDescricaoLote() {
		return descricaoLote;
	}

	public void setDescricaoLote(String descricaoLote) {
		this.descricaoLote = descricaoLote;
	}
}
