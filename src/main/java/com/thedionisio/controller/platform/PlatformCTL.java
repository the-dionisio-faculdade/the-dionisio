package com.thedionisio.controller.platform;

import com.thedionisio.model.DAO.EventoDAO;
import com.thedionisio.model.DAO.InteresseDAO;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DAO.PessoaGeneroDAO;
import com.thedionisio.model.DTO.EventoDTO;
import com.thedionisio.model.DTO.InteresseDTO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.model.DTO.PessoaGeneroDTO;

import groovy.util.Factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@Component
@Controller
public class PlatformCTL {

	@Autowired
	PessoaGeneroDAO pessoaGeneroDAO;
	@Autowired
	EventoDAO eventoDAO;
	@Autowired
	PessoaDAO pessoaDAO;
	@Autowired
	InteresseDAO interesseDAO;

	@RequestMapping("/Platform")
	public ModelAndView IndexPlatform(PessoaDTO user) {
		ModelAndView mv = new ModelAndView("/platform/Platform");
		PessoaGeneroDTO genres = new PessoaGeneroDTO();
		//ArrayList<EventoDTO> eventos = eventoDAO.findByEventoidPessoa(3l);
		ArrayList<PessoaGeneroDTO>pessoaGenero = pessoaGeneroDAO.findByidPessoa(user.getIdPessoa());
		mv.addObject("user",user);
		mv.addObject("pessoaGenero",pessoaGenero);
		mv.addObject("genres",genres);
		//mv.addObject("edto", eventos);
		return mv;
	}
	

	@RequestMapping(value = "/Evento/{nomeEvento:.+}&{idEvento:.+}", method = RequestMethod.GET)
	public ModelAndView Events(@PathVariable String nomeEvento,
							  @PathVariable String idEvento)
	{
		long idEvent = Long.parseLong(idEvento);
		ModelAndView mv = new ModelAndView("/platform/Event");
		PessoaDTO user = new PessoaDTO();
		EventoDTO evento = new EventoDTO();
		PessoaGeneroDTO genres = new PessoaGeneroDTO();
		evento.setIdEvento(idEvent);
		ArrayList<PessoaGeneroDTO>pessoaGenero = pessoaGeneroDAO.findByidPessoa(user.getIdPessoa());
		
		mv.addObject("user",user);
		mv.addObject("pessoaGenero",pessoaGenero);
		mv.addObject("genres",genres);
		mv.addObject("evento", evento);
		return mv;
	}

	@RequestMapping(value = "/Empresa/{nomeEmpresa:.+}", method = RequestMethod.GET)
	public ModelAndView viewEstabelicimento(@PathVariable String nomeEmpresa){
		ModelAndView mv = new ModelAndView("/platform/Company");
		PessoaDTO user = new PessoaDTO();
		PessoaGeneroDTO genres = new PessoaGeneroDTO();
		ArrayList<PessoaGeneroDTO>pessoaGenero = pessoaGeneroDAO.findByidPessoa(user.getIdPessoa());
		mv.addObject("user",user);
		mv.addObject("pessoaGenero",pessoaGenero);
		mv.addObject("genres",genres);

		return mv;
	}
	
	
	/*Busca os eventos no banco e popula o mapa da platfrom */
	@ResponseBody
	@RequestMapping(value = "/BuscarEventoGenero", method =RequestMethod.POST)
	public ArrayList<EventoDTO> buscarEventoGenero(@RequestParam String lat,
							   				 @RequestParam String lon,
							   				 @RequestParam String email)
	{
		PessoaDTO pessoa = pessoaDAO.findByEmail(email);
		ArrayList<EventoDTO> eventos = eventoDAO.findByEventoidPessoa(pessoa.getIdPessoa());
		return eventos;
	}
	
	@ResponseBody
	@RequestMapping(value = "/ListarEventoGenero", method =RequestMethod.POST)
	public ArrayList<EventoDTO> buscarEventoGenero(@RequestParam String email)
	{
		PessoaDTO pessoa = pessoaDAO.findByEmail(email);
		ArrayList<EventoDTO> eventos = eventoDAO.findByEventoidPessoa(pessoa.getIdPessoa());
		return eventos;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/ListarEventoGeneroLike", method =RequestMethod.POST)
	public ArrayList<EventoDTO> buscarEventoGeneroLike(@RequestParam String email)
	{
		PessoaDTO pessoa = pessoaDAO.findByEmail(email);
		ArrayList<EventoDTO> eventos = eventoDAO.findByEventoInteresse(pessoa.getIdPessoa());
		return eventos;
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/BuscarEvento", method = RequestMethod.POST)
	public EventoDTO buscarEvento(@RequestParam String idEvento){
		System.out.println("Cheguei aqui o id é " + idEvento);
		long id = Long.parseLong(idEvento);
		EventoDTO evento = eventoDAO.findByidEvento(id);
		return evento;
	}
	
	@ResponseBody
	@RequestMapping(value = "/Interesse", method = RequestMethod.POST)
	public String interesse(@RequestParam String email,
							@RequestParam String idEvento,
							@RequestParam String interesse,
							@RequestParam String dt)
	{
		InteresseDTO idto = new InteresseDTO();
		PessoaDTO userResult = pessoaDAO.findByEmail(email);
		long idEvent = Long.parseLong(idEvento);
		if(interesse.equals("0")){
			idto.setNotLike(true);
		}else{
			idto.setLikes(true);
		}
		idto.setIdEvento(idEvent);
		idto.setIdPessoa(userResult.getIdPessoa());
		idto.setData(dt);
		
		InteresseDTO inDTO = interesseDAO.findByidInteresse(userResult.getIdPessoa(), idEvent);
		if(inDTO != null){
			interesseDAO.delete(inDTO);
			interesseDAO.save(idto);
		}else{
			interesseDAO.save(idto);
		}
		return "Deu certo";
	}
	
}
