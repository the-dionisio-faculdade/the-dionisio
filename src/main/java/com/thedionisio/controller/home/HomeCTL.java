
package com.thedionisio.controller.home;


import com.thedionisio.model.DAO.PessoaGeneroDAO;
import com.thedionisio.model.DTO.EmailDTO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.service.email.Email;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.SystemException;
import java.net.MalformedURLException;


@Controller
public class HomeCTL {

	private Email Email = new Email();

	@Autowired
	PessoaGeneroDAO pessoaGeneroDAO;

	@RequestMapping("/Home")
	public ModelAndView Home() throws MalformedURLException, EmailException {

		PessoaDTO user = new PessoaDTO();
	  	EmailDTO email = new EmailDTO();
		ModelAndView mv = new ModelAndView("/home/Home");
		mv.addObject("user", user);
		mv.addObject("email", email);
		return mv;
	}



	@RequestMapping("/")
	public ModelAndView HomeAll() throws IllegalStateException, SystemException {
		PessoaDTO user = new PessoaDTO();
	  	EmailDTO email = new EmailDTO();
		ModelAndView mv = new ModelAndView("/home/Home");
		mv.addObject("user", user);
		mv.addObject("email", email);
		return mv;
	}
	

    //Metodo para solicitar nova senha
    @RequestMapping(value = "/home/Email", method = RequestMethod.POST)
    public void EnviaEmail(EmailDTO e) throws EmailException
    {
    	Email.EnviarTextoSimples(e.getEmailRemetente(), e.getAssunto(), e.getAssunto());
    }

	
	
}
