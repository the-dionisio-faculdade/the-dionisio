package com.thedionisio.controller.dash.checkin;

import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DTO.PessoaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by jonathan on 22/10/16.
 */

@Controller
public class CheckinCTL {

    @Autowired
    private PessoaDAO pessoaDAO;

    @RequestMapping(value = "/Checkin", method = RequestMethod.POST)
    public ModelAndView ControleEntradaEnvento(PessoaDTO pessoa) {
        PessoaDTO user = pessoaDAO.findOne(pessoa.getIdPessoa());
        ModelAndView mv = new ModelAndView("/dash/Checkin");
        mv.addObject("user", user);
        return mv;
    }
}
