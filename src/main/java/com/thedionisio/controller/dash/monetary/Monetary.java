package com.thedionisio.controller.dash.monetary;

import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DTO.PessoaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by jonathan on 22/10/16.
 */
@Controller
public class Monetary {

    @Autowired
    private PessoaDAO pessoaDAO;

    @RequestMapping(value = "/Monetario", method = RequestMethod.POST)
    public ModelAndView Monetary(PessoaDTO pessoa) {
        PessoaDTO user = pessoaDAO.findOne(pessoa.getIdPessoa());
        ModelAndView mv = new ModelAndView("/dash/Monetary");
        mv.addObject("user", user);
        return mv;
    }
}
