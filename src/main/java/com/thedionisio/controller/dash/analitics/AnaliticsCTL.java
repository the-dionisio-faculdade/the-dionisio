package com.thedionisio.controller.dash.analitics;

import com.thedionisio.model.DAO.EmpresaDAO;
import com.thedionisio.model.DAO.InteresseDAO;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DTO.EmpresaDTO;
import com.thedionisio.model.DTO.PessoaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by jonathan on 22/10/16.
 */

@Controller
public class AnaliticsCTL {

    @Autowired
    private PessoaDAO pessoaDAO;
    @Autowired
    private EmpresaDAO empresaDAO;
    @Autowired
    InteresseDAO interesseDAO;

    @RequestMapping(value = "/Analitico", method = RequestMethod.POST)
    public ModelAndView Analitics(PessoaDTO pessoa) {

        PessoaDTO user = pessoaDAO.findOne(pessoa.getIdPessoa());
        ModelAndView mv = new ModelAndView("/dash/Analitics");
        ArrayList<EmpresaDTO> filiais = empresaDAO.findByidPessoa(pessoa.getIdPessoa());
        mv.addObject("user", user);
        mv.addObject("filiais", filiais);




        /*mv.addObject("tempo", "'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'");
        mv.addObject("linhaM", "7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6");
        mv.addObject("linhaF", "3.9, 12.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8");
        mv.addObject("linhaT", "10.9, 19.1, 16.2, 23, 30.03, 36.7, 43.2, 43.1, 37.5, 28.6, 18.5, 15.4");
        mv.addObject("nomeFesta", "Vaca Loka");*/

        return mv;
    }

    @ResponseBody
    @RequestMapping("/BuscaInteresse")
    public ArrayList<String> buscaInteresse(@RequestParam String idEvento){
        ArrayList<String> teste = new ArrayList<>();
        teste.add("'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'");
        teste.add("7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6");
        teste.add("3.9, 12.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8");
        teste.add("10.9, 19.1, 16.2, 23, 30.03, 36.7, 43.2, 43.1, 37.5, 28.6, 18.5, 15.4");
        teste.add("Vaca Loka");

        //ArrayList<InteresseDTO>a = interesseDAO.findByinEvento(Long.parseLong(idEvento));

        return teste;
    }
}
