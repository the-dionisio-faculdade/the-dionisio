package com.thedionisio.controller.dash.about;

import com.thedionisio.model.DAO.EmpresaDAO;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DAO.RedeSocialEmpresaDAO;
import com.thedionisio.model.DTO.EmpresaDTO;
import com.thedionisio.model.DTO.MessageDTO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.model.DTO.RedeSocialEmpresaDTO;
import com.thedionisio.service.enums.OperacaoCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 22/10/16.
 */
@Controller
public class AboutCTL {
    /* CREATED BY IGOR ON 10/10/16 */
    /* CRUD DO ABOUT DA EMPRESA AQUI */

    @Autowired
    private EmpresaDAO empresaDAO;

    @Autowired
    private RedeSocialEmpresaDAO redeSocialEmpresaDAO;

    @Autowired
    private PessoaDAO pessoaDAO;

    /* AQUI LISTAGEM DO ABOUT DAS EMPRESAS */
    @RequestMapping(value = "/Informacao", method = RequestMethod.POST)
    public ModelAndView Sobre(PessoaDTO pessoa)
    {
      ModelAndView mv = new ModelAndView("/dash/About");
      EmpresaDTO filial = new EmpresaDTO();
      mv.addObject("filial", filial);

      PessoaDTO user = pessoaDAO.findOne(pessoa.getIdPessoa());
      mv.addObject("user", user);

      MessageDTO msg = new MessageDTO();
      msg.setTipo(OperacaoCrud.none);
      mv.addObject("msg",msg);

      filial.setIdPessoa(pessoa.getIdPessoa());

      ArrayList<EmpresaDTO> filiais = empresaDAO.findByidPessoa(user.getIdPessoa());
      mv.addObject("filiais", filiais);

      return mv;
    }

    /* AQUI SALVA O ABOUT  DA EMPRESA - OBS: NÃO É UM CRUD, APENAS UPDATE */
    @SuppressWarnings("static-access")
	@RequestMapping(value = "/Sobre", method = RequestMethod.POST)
    public ModelAndView SalvaAboutFilial(EmpresaDTO filial) throws NoSuchFieldException, IllegalAccessException
    {
        ModelAndView mv = new ModelAndView("/dash/About");

        MessageDTO msg = new MessageDTO();
        msg.setTipo(filial.getOp());
        msg.setTexto(filial.getNomeFant().toLowerCase());

        System.out.println(filial.getOp());
        if (filial.getOp().equals(OperacaoCrud.update))
        {
            EmpresaDTO filialFull = empresaDAO.findByidEmpresa(filial.getIdEmpresa());
            filial = (EmpresaDTO) empresaDAO.up.date(filialFull, filial);
            empresaDAO.save(filial);
        }
        else
        {
            msg.setTipo(OperacaoCrud.fail);
            msg.setTexto(filial.getNomeFant().toLowerCase());
        }

        EmpresaDTO newFilial = new EmpresaDTO();
        newFilial.setIdPessoa(filial.getIdPessoa());
        mv.addObject("filial", newFilial);

        PessoaDTO user = pessoaDAO.findOne(filial.getIdPessoa());
        mv.addObject("user", user);

        mv.addObject("msg", msg);

        ArrayList<EmpresaDTO> filiais = empresaDAO.findByidPessoa(filial.getIdPessoa());
        mv.addObject("filiais", filiais);

        return mv;
    }

    /* AJAX DE BUSCA DO ABOUT */
    @RequestMapping(value = "/About/Ajax", method = RequestMethod.POST)@ResponseBody
    public EmpresaDTO BuscaSobreFilial(@RequestParam String id)
    {
        long x = Long.parseLong(id);
        EmpresaDTO empresaResult = empresaDAO.findByidEmpresa(x);
        return empresaResult;
    }
    
    /* CREATED BY IGOR ON 05/11/16 */
    /* CRUD DA REDE SOCIAL AQUI */

    /* AJAX DE BUSCA DA REDE SOCIAL (CAMPOS) */
    @RequestMapping(value = "/RedeSocialEmpresa/Ajax", method = RequestMethod.POST)@ResponseBody
    public ArrayList<RedeSocialEmpresaDTO> BuscaRedeSocialFilial(@RequestParam String id)
    {
        long x = Long.parseLong(id);
        ArrayList<RedeSocialEmpresaDTO> redeSocialEmpresaResult = redeSocialEmpresaDAO.findByidEmpresa(x);
        return redeSocialEmpresaResult;
    }

    /* AJAX COM CRUD DA REDESOCIALEMPRESA BY: JONATHAN AND IGOR */
    @RequestMapping(value = "/SalvaRedeSocial", method = RequestMethod.POST)@ResponseBody
    public Boolean SalvaRedeSocial(@RequestParam("listaUrlComplementar[]") List<String> listaUrlComplementar)
    {
    	ModelAndView mvr = new ModelAndView("/dash/About");

        Long idEmpresa = Long.parseLong(listaUrlComplementar.get(0));
        List<RedeSocialEmpresaDTO> lista = redeSocialEmpresaDAO.findByidEmpresa(idEmpresa);

        MessageDTO msg = new MessageDTO();
        msg.setTipo("updateRede");
        mvr.addObject("msg",msg);

        lista.forEach((redeSocialEmpresa) ->{
            redeSocialEmpresaDAO.delete(redeSocialEmpresa);
        });

        for (int i =1; i<listaUrlComplementar.size();i++){
            RedeSocialEmpresaDTO redeSocialEmpresaDTO = new RedeSocialEmpresaDTO();
            redeSocialEmpresaDTO.setIdEmpresa(idEmpresa);
            redeSocialEmpresaDTO.setIdRede(Long.parseLong(listaUrlComplementar.get(i)));
            i++;
            redeSocialEmpresaDTO.setUrlComplementar(listaUrlComplementar.get(i));
            redeSocialEmpresaDAO.save(redeSocialEmpresaDTO);
        }

		return true;
    }
}
