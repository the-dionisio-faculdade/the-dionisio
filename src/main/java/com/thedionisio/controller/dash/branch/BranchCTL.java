package com.thedionisio.controller.dash.branch;

import com.thedionisio.model.DAO.EmpresaDAO;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DTO.EmpresaDTO;
import com.thedionisio.model.DTO.MessageDTO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.service.enums.OperacaoCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by jonathan on 22/10/16.
 */
@Component
@Controller
public class BranchCTL {

    @Autowired
    private EmpresaDAO empresaDAO;
    @Autowired
    private PessoaDAO pessoaDAO;

    public ModelAndView Filial(PessoaDTO user) throws NoSuchFieldException, IllegalAccessException {

        ModelAndView mv = new ModelAndView("/dash/Branch");
        EmpresaDTO filial = new EmpresaDTO();
        mv.addObject("filial", filial);
        mv.addObject("user", user);
        MessageDTO msg = new MessageDTO();
        msg.setTipo(OperacaoCrud.none);
        mv.addObject("msg",msg);

        filial.setIdPessoa(user.getIdPessoa());

        ArrayList<EmpresaDTO> filiais  = empresaDAO.findByidPessoa(user.getIdPessoa());
        mv.addObject("filiais",filiais);


        return mv;
    }

    @RequestMapping(value = "/Filiais", method = RequestMethod.POST)
    public ModelAndView goFiliais(PessoaDTO adm) throws NoSuchFieldException, IllegalAccessException {

        ModelAndView mv = new ModelAndView("/dash/Branch");
        EmpresaDTO filial = new EmpresaDTO();
        PessoaDTO user = pessoaDAO.findOne(adm.getIdPessoa());
        mv.addObject("filial", filial);
        mv.addObject("user", user);
        MessageDTO msg = new MessageDTO();
        msg.setTipo(OperacaoCrud.none);
        mv.addObject("msg",msg);

        filial.setIdPessoa(adm.getIdPessoa());

        ArrayList<EmpresaDTO> filiais  = empresaDAO.findByidPessoa(user.getIdPessoa());
        mv.addObject("filiais",filiais);


        return mv;
    }

    @RequestMapping(value = "/Filial", method = RequestMethod.POST)
    public ModelAndView SalvaFilial(EmpresaDTO filial) throws NoSuchFieldException, IllegalAccessException {
        ModelAndView mv = new ModelAndView("/dash/Branch");
        EmpresaDTO empresaResult = new EmpresaDTO();

        MessageDTO msg = new MessageDTO();
        msg.setTipo(filial.getOp());
        msg.setTexto(filial.getNomeFant().toLowerCase());


        if(filial.getOp().equals(OperacaoCrud.delete))
        {
            empresaDAO.delete(filial);
        }
        else if(filial.getOp().equals(OperacaoCrud.update))
        {
            filial.setIdRamo(1l);
            EmpresaDTO filialFull = empresaDAO.findByidEmpresa(filial.getIdEmpresa());
            filial = (EmpresaDTO) empresaDAO.up.date(filialFull, filial);
            empresaDAO.save(filial);
        }
        else{
            empresaResult= empresaDAO.findByCnpj(filial.getCnpj());
            if (empresaResult==null){
                filial.setIdRamo(1l);
                empresaDAO.save(filial);
                msg.setTipo(OperacaoCrud.save);
            }
            else{
                msg.setTipo(OperacaoCrud.fail);
                msg.setTexto(filial.getCnpj());
            }
        }

        EmpresaDTO newFilial = new EmpresaDTO();
        PessoaDTO user = pessoaDAO.findOne(filial.getIdPessoa());

        newFilial.setIdPessoa(filial.getIdPessoa());

        mv.addObject("filial", filial);
        mv.addObject("user", user);
        mv.addObject("msg",msg);

        ArrayList<EmpresaDTO> filiais  = empresaDAO.findByidPessoa(newFilial.getIdPessoa());
        mv.addObject("filiais",filiais);

        return mv;
    }

    @RequestMapping(value = "/Branch/Ajax", method = RequestMethod.POST)@ResponseBody
    public EmpresaDTO BuscaFilial(@RequestParam String id)
    {

        long x = Long.parseLong(id);
        EmpresaDTO empresaResult = empresaDAO.findByidEmpresa(x);
        return empresaResult;

    }

}
