package com.thedionisio.controller.dash.events;


import com.thedionisio.model.DAO.*;
import com.thedionisio.model.DTO.*;
import com.thedionisio.service.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 22/10/16.
 */

@Controller
public class EventoCTL {
	
	@Autowired
	private EmpresaDAO empresaDAO;
	@Autowired
	private EventoDAO eventoDAO;
	@Autowired
	private LoteDAO loteDAO;
	@Autowired
	private SetorDAO setorDAO;
	@Autowired
	private IngressoDAO ingressoDAO;
	@Autowired
	private PessoaDAO pessoaDAO;
    @Autowired
    private EventoGeneroDAO eventoGeneroDAO;

	private final StorageService storageService;

	@Autowired
	public EventoCTL(StorageService storageService) {
		this.storageService = storageService;
	}


	@RequestMapping( value = "/Evento", method = RequestMethod.POST)
    public ModelAndView evento(PessoaDTO pessoa)
    {
        EventoDTO eventoDTO = new EventoDTO();
		IngressoDTO ingressoDTO = new IngressoDTO();
		LoteDTO loteDTO = new LoteDTO();
        ModelAndView mv = new ModelAndView("/dash/Events");
		PessoaDTO user = pessoaDAO.findOne(pessoa.getIdPessoa());
        ArrayList<EmpresaDTO> filiais = empresaDAO.findByidPessoa(pessoa.getIdPessoa());
		ArrayList<SetorDTO> setores = (ArrayList<SetorDTO>) setorDAO.findAll();
		ArrayList<EventoDTO> eventos =  eventoDAO.findByidEstabelecimento(pessoa.getIdPessoa());
		mv.addObject("eventos", eventos);
		mv.addObject("filiais", filiais);
		mv.addObject("setores", setores);
		mv.addObject("eventoDTO", eventoDTO);
		mv.addObject("loteDTO", loteDTO);
		mv.addObject("ingressoDTO", ingressoDTO);
		mv.addObject("user", user);
        mv.addObject("msg","none");
        return mv;
    }

	@ResponseBody
    @RequestMapping(value = "/SalvaInfo", method = RequestMethod.POST)
	public EventoDTO salvaInfo(@RequestParam String idEvento,
						  @RequestParam String nomeEvento,
						  @RequestParam String dataInicialEvento,
						  @RequestParam String dataFinalEvento,
						  @RequestParam String descricaoEvento,
						  @RequestParam String idEstabelecimento,
						  @RequestParam boolean updateEvento) {


		EventoDTO eventoResult = eventoDAO.findOne(Long.parseLong(idEvento));

		EventoDTO eventoDTO = new EventoDTO();

		if(eventoResult!=null){

			eventoDTO = eventoResult;
		}

		if (eventoResult==null || updateEvento==true){

			eventoDTO.setNomeEvento(nomeEvento);
			eventoDTO.setDataInicialEvento(dataInicialEvento);
			eventoDTO.setDataFinalEvento(dataFinalEvento);
			eventoDTO.setDescricaoEvento(descricaoEvento);
			eventoDTO.setIdEstabelecimento(Long.parseLong(idEstabelecimento));
			eventoDAO.save(eventoDTO);


			eventoDTO = eventoDAO.findOne(eventoDTO.getIdEvento());

			return eventoDTO;
		}

		return null;
    }

	@ResponseBody
	@RequestMapping(value = "/SalvaLote", method = RequestMethod.POST)
	public List<LoteDTO> salvaLote(@RequestParam String idLote,
								   @RequestParam String idEvento,
								   @RequestParam String dataInicialLote,
								   @RequestParam String dataFinalLote,
								   @RequestParam String descricaoLote,
								   @RequestParam boolean updateLote) {

		LoteDTO loteResult = loteDAO.findByLote(Long.parseLong(idEvento),
												dataInicialLote,
												dataFinalLote,
												descricaoLote);


		if (updateLote){
			LoteDTO loteUpdate = new LoteDTO();
			loteUpdate.setIdLote(Long.parseLong(idLote));
			loteUpdate.setDataInicialLote(dataInicialLote);
			loteUpdate.setDataFinalLote(dataFinalLote);
			loteUpdate.setDescricaoLote(descricaoLote);
			loteUpdate.setIdEvento(Long.parseLong(idEvento));

			loteDAO.save(loteUpdate);

			return loteDAO.findByidEvento(Long.parseLong(idEvento));
		}

		if (loteResult == null) {

			LoteDTO loteDTO = new LoteDTO();
			loteDTO.setIdEvento(Long.parseLong(idEvento));
			loteDTO.setDataInicialLote(dataInicialLote);
			loteDTO.setDataFinalLote(dataFinalLote);
			loteDTO.setDescricaoLote(descricaoLote);

			loteDAO.save(loteDTO);

	}

	return loteDAO.findByidEvento(Long.parseLong(idEvento));
	}

	@ResponseBody
	@RequestMapping(value = "/SalvaIngresso", method = RequestMethod.POST)
	public List<IngressoDTO>  salvaIngresso(@RequestParam String idLote,
										    @RequestParam String idSetor,
										    @RequestParam String idIngresso,
										    @RequestParam String idEvento,
										    @RequestParam String qtdMasculino,
										    @RequestParam String valorMasculino,
										    @RequestParam String qtdFeminino,
										    @RequestParam String valorFeminino,
										    @RequestParam String qtdUnico,
										    @RequestParam String valorUnico,
										    @RequestParam boolean estudante,
										    @RequestParam boolean disticaoSexo,
										    @RequestParam boolean openBar,
										    @RequestParam boolean updateIngresso){

		IngressoDTO ingressoResult = ingressoDAO.findByIngresso(Long.parseLong(idLote),
															    Long.parseLong(idSetor),
															    Long.parseLong(idEvento));

		IngressoDTO ingressoDTO = new IngressoDTO();

		if(updateIngresso){

			ingressoDTO.setIdIngresso(Long.parseLong(idIngresso));
		}


		if (ingressoResult == null || updateIngresso== true){

			ingressoDTO.setIdLote(Long.parseLong(idLote));
			ingressoDTO.setIdSetor(Long.parseLong(idSetor));
			ingressoDTO.setIdEvento(Long.parseLong(idEvento));
			ingressoDTO.setQtdMasculino(qtdMasculino);
			ingressoDTO.setValorMasculino(valorMasculino);
			ingressoDTO.setQtdFeminino(qtdFeminino);
			ingressoDTO.setValorFeminino(valorFeminino);
			ingressoDTO.setQtdUnico(qtdUnico);
			ingressoDTO.setValorUnico(valorUnico);
			ingressoDTO.setEstudante(estudante);
			ingressoDTO.setDisticaoSexo(disticaoSexo);
			ingressoDTO.setOpenBar(openBar);

			ingressoDAO.save(ingressoDTO);
		}

		ArrayList<IngressoDTO> listIngresso  = ingressoDAO.findByidEvento(Long.parseLong(idEvento));

		listIngresso.forEach((l)->{
			l.setDescricaoLote(loteDAO.findOne(l.getIdLote()).getDescricaoLote());
			l.setDescricaoSetor(setorDAO.findOne(l.getIdSetor()).getDescricaoSetor());
		});


		return listIngresso;
	}

	@ResponseBody
	@RequestMapping(value="/DeletaLote", method = RequestMethod.POST)
	public LoteDTO deletaLote(@RequestParam String idLote){

		ArrayList<IngressoDTO> ingressosDesseLote = ingressoDAO.findByidLote(Long.parseLong(idLote));

		ingressosDesseLote.forEach((i) ->{
			ingressoDAO.delete(i);
		});

		loteDAO.delete(Long.parseLong(idLote));

		LoteDTO loteDTO = new LoteDTO();
		loteDTO.setIdLote(Long.parseLong(idLote));
		loteDTO.setIngressos(ingressosDesseLote);


		return loteDTO;
	}

	@ResponseBody
	@RequestMapping(value="/DeletaIngresso", method = RequestMethod.POST)
	public String deletaIngresso(@RequestParam String idIngresso){

		ingressoDAO.delete(Long.parseLong(idIngresso));

		return idIngresso;
	}

	@ResponseBody
	@RequestMapping(value="/BuscaLote", method = RequestMethod.POST)
	public LoteDTO buscaLote(@RequestParam String idLote){

		return loteDAO.findOne(Long.parseLong(idLote));

	}

    @ResponseBody
    @RequestMapping(value="/BuscaEnderecoFilial", method = RequestMethod.POST)
    public EmpresaDTO buscaEnderecoFilial(@RequestParam String idFilial){

        return empresaDAO.findOne(Long.parseLong(idFilial));

    }

	@ResponseBody
	@RequestMapping(value="/BuscaIngresso", method = RequestMethod.POST)
	public IngressoDTO buscaIngresso(@RequestParam String idIngresso){

		return ingressoDAO.findOne(Long.parseLong(idIngresso));

	}

	@ResponseBody
	@RequestMapping(value="/BuscaEvento", method = RequestMethod.POST)
	public ArrayList<EventoDTO> buscaEvento(@RequestParam String idEmpresa,
                                            @RequestParam String idPessoa,
                                            @RequestParam boolean all){

		ArrayList<EventoDTO> eventos = new ArrayList<>();

		if (all){
			ArrayList<EmpresaDTO> empresas = empresaDAO.findByidPessoa(Long.parseLong(idPessoa));

            for (EmpresaDTO f : empresas) {

                ArrayList<EventoDTO> eventoFilial = eventoDAO.findByidEstabelecimento(f.getIdEmpresa());

                for (EventoDTO e : eventoFilial) {
                    eventos.add(e);
                }

            }
		}
		else {
            eventos = eventoDAO.findByidEstabelecimento(Long.parseLong(idEmpresa));
        }


		return eventos;

	}

	@ResponseBody
	@RequestMapping("/PopulaLoteIngresso")
	public ArrayList PopulaLoteIngresso(@RequestParam String idEvento) {


		ArrayList listaLoteIngresso = new ArrayList();
		EventoDTO eventoDTO = eventoDAO.findOne(Long.parseLong(idEvento));


		ArrayList<LoteDTO> lotes = loteDAO.findByidEvento(Long.parseLong(idEvento));
		ArrayList<IngressoDTO> ingressos = ingressoDAO.findByidEvento(Long.parseLong(idEvento));

		listaLoteIngresso.add(eventoDTO);
		listaLoteIngresso.add(lotes);

        ingressos.forEach((l)->{
            l.setDescricaoLote(loteDAO.findOne(l.getIdLote()).getDescricaoLote());
            l.setDescricaoSetor(setorDAO.findOne(l.getIdSetor()).getDescricaoSetor());
        });

        listaLoteIngresso.add(ingressos);

        ArrayList<EventoGeneroDTO> generos = eventoGeneroDAO.findByidEvento(Long.parseLong(idEvento));


        listaLoteIngresso.add(generos);

		return  listaLoteIngresso;

	}

	@ResponseBody
	@RequestMapping("/DeletarEvento")
	public String DeletarEvento(@RequestParam String idEvento) {

		ArrayList<IngressoDTO> ingressos = ingressoDAO.findByidEvento(Long.parseLong(idEvento));
		ArrayList<LoteDTO> lotes = loteDAO.findByidEvento(Long.parseLong(idEvento));
		EventoDTO evento = eventoDAO.findOne(Long.parseLong(idEvento));

		ingressos.forEach((i)->{
			ingressoDAO.delete(i);
		});

		lotes.forEach((l)->{
			loteDAO.delete(l);
		});

		eventoDAO.delete(evento);

		return idEvento;

	}


	@ResponseBody
	@RequestMapping("/SalvaEnderecoEvento")
	public String salvaEndereco(@RequestParam String idEvento,
                                @RequestParam String cep,
                                @RequestParam String cid,
                                @RequestParam String num,
                                @RequestParam String log,
                                @RequestParam String latitude,
                                @RequestParam String longitude) {

        EventoDTO eventoDTO = eventoDAO.findOne(Long.parseLong(idEvento));

        if (eventoDTO!=null){
            eventoDTO.setCep(cep);
            eventoDTO.setCidade(cid);
            eventoDTO.setNumero(num);
            eventoDTO.setLogradouro(log);
            eventoDTO.setLatitude(latitude);
            eventoDTO.setLongitude(longitude);

            eventoDAO.save(eventoDTO);
        }

        return "";
	}

	@ResponseBody
    @RequestMapping("/AtualizaGeneroEvento")
    public void atuzalizaGeneroEvento(@RequestParam String idEvento,
                                      @RequestParam String genero){
        try{

            ArrayList<EventoGeneroDTO> listGenero = eventoGeneroDAO.findByidEvento(Long.parseLong(idEvento));

            listGenero.forEach((e)->{
                eventoGeneroDAO.delete(e);
            });

            String[] generos = null;
            generos = genero.split(",");
            for (String g : generos) {
                if (!g.equals("0")){
                    EventoGeneroDTO eventoGeneroDTO = new EventoGeneroDTO();
                    eventoGeneroDTO.setIdGenero(Long.parseLong(g));
                    eventoGeneroDTO.setIdEvento(Long.parseLong(idEvento));
                    eventoGeneroDAO.save(eventoGeneroDTO);
                }
            }
        }
        catch (Exception e){

        }
    }

	@ResponseBody
	@RequestMapping("/Up")
	public String handleFileUpload(@RequestParam("file") MultipartFile file){
		storageService.store(file);
		return "";
	}

}
