package com.thedionisio.controller.company;

import com.thedionisio.controller.dash.branch.BranchCTL;
import com.thedionisio.model.DAO.EmpresaDAO;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DAO.RamoDAO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.model.DTO.RamoDTO;
import com.thedionisio.service.Password;
import com.thedionisio.service.email.Email;
import com.thedionisio.service.email.TemplateEmail;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


@Controller
public class CompanyCTL {

	@Autowired 
	private PessoaDAO pessoaDAO;
	@Autowired 
	private EmpresaDAO empresaDAO;
	@Autowired
	private RamoDAO ramoDAO;
	@Autowired
	BranchCTL branchCTL;
	Password password = new Password();

	Email email = new Email();


	@RequestMapping("/ParaEmpresa")
	public ModelAndView Empresa() {

		PessoaDTO user = new PessoaDTO();
		ArrayList<RamoDTO> ramos = (ArrayList<RamoDTO>) ramoDAO.findAll();
		ModelAndView mv = new ModelAndView("/company/Contact");
		mv.addObject("user", user);


		return mv;
	}

	@RequestMapping(value = "/ParaEmpresa", method = RequestMethod.POST)
	public ModelAndView CadastroInicialEmpresa(PessoaDTO user) throws MalformedURLException, EmailException, NoSuchAlgorithmException {

		PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());
		String senha = password.GerarPassword();

		if(userResult == null)
		{
			user.setIsCompany(true);
			user.setSenha(password.Criptografar(senha));
			pessoaDAO.save(user);
		}
		else
		{
			System.out.println("E-mail já cadastrado !");
		}

		email.EnviarTextoHTML(user.getEmail(),
				             "Bem Vindo ao TheDionidio",
				              TemplateEmail.get.welcomeCompany(user.getNomePessoa(), senha),
						     "Bem Vindo ao TheDionidio");

		ModelAndView mv = new ModelAndView("/company/Contact-out");
		return mv;
	}

	@RequestMapping("/LoginEmpresa")
	public ModelAndView LoginEmpresa() {
		ModelAndView mv = new ModelAndView("/company/Login");
		PessoaDTO user = new PessoaDTO();
		mv.addObject("user", user);
		return mv;
	}
	

	@RequestMapping(value ="/LoginEmpresa", method = RequestMethod.POST)
	public ModelAndView LoginEmpresaCadastrada(PessoaDTO user) throws NoSuchAlgorithmException, NoSuchFieldException, IllegalAccessException{
		
		PessoaDTO userResult =  pessoaDAO.findByEmail(user.getEmail());

		if(userResult != null && userResult.getIsCompany()){
			if(password.Valida(user.getSenha(), userResult.getSenha())){

		        return branchCTL.Filial(userResult);
			}
		}
		
		return LoginEmpresa();
	}

	@RequestMapping(value ="/AtualizarUserCompany", method = RequestMethod.POST)
	public ModelAndView AtualizarUserCompany(PessoaDTO user) throws NoSuchAlgorithmException, NoSuchFieldException, IllegalAccessException{

		PessoaDTO userResult =  pessoaDAO.findByEmail(user.getEmail());

		if(userResult != null && userResult.getIsCompany()){

			if(user.getSenha() !=null && password.Valida(user.getSenha(), userResult.getSenha()) && user.getNovaSenha() != null) {

				userResult.setSenha(password.Criptografar(user.getNovaSenha()));

			}
			else {
				userResult.setNomePessoa(user.getNomePessoa());
				userResult.setTelefoneCelular(user.getTelefoneCelular());
				userResult.setTelefoneFixo(user.getTelefoneFixo());
			}



			pessoaDAO.save(userResult);

			userResult = pessoaDAO.findOne(userResult.getIdPessoa());
		}


		return branchCTL.Filial(userResult);
	}


}
