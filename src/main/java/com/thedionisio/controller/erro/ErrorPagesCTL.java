package com.thedionisio.controller.erro;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorPagesCTL {

	@RequestMapping("/404")
	public String notFound() {
		return "/error/404";
	}
	
	@RequestMapping("/403")
	public String forbidden() {
		return "/error/403";
	}
	
	@RequestMapping("/500")
	public String internalServerError() {
		return "/error/500";
	}

	@RequestMapping("/400")
	public String rError() {
		return "/error/500";
	}
}
