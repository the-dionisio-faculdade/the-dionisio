package com.thedionisio.controller.account;

import com.thedionisio.controller.platform.PlatformCTL;
import com.thedionisio.model.DAO.PessoaDAO;
import com.thedionisio.model.DAO.PessoaGeneroDAO;
import com.thedionisio.model.DTO.PessoaDTO;
import com.thedionisio.model.DTO.PessoaGeneroDTO;
import com.thedionisio.service.Password;
import com.thedionisio.service.email.Email;
import com.thedionisio.service.email.TemplateEmail;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


@Controller
public class AccountCTL
{

    @Autowired
    private PessoaDAO pessoaDAO;
    @Autowired
    private PessoaGeneroDAO pessoaGeneroDAO;
    @Autowired
    private PlatformCTL platformCTL;

    private Email email = new Email();
    private Password password = new Password();

    //Somente Chamadas de Page
    @RequestMapping("/EsqueciMinhaSenha")
    public ModelAndView EsqueciMinhaSenha()
    {
        PessoaDTO user = new PessoaDTO();
        ModelAndView mv = new ModelAndView("/account/Forgot-Password");
        mv.addObject("user", user);
        return mv;
    }

    @RequestMapping("/Cadastro")
    public ModelAndView CadastrarUser()
    {
    	PessoaDTO user = new PessoaDTO();
        PessoaGeneroDTO genres = new PessoaGeneroDTO();
        ModelAndView mv = new ModelAndView("/account/Sing-Up");
        mv.addObject("user", user);
        mv.addObject("genres", genres);
        return mv;
    }

    public ModelAndView CadastrarUser(PessoaDTO user)
    {
        ModelAndView mv = new ModelAndView("/account/Sing-Up");
        PessoaGeneroDTO genres = new PessoaGeneroDTO();
        mv.addObject("user", user);
        mv.addObject("genres", genres);
        return mv;
    }

    @RequestMapping("/Login")
    public ModelAndView Login()
    {
    	PessoaDTO user = new PessoaDTO();
        ModelAndView mv = new ModelAndView("/account/Sing-In");
        mv.addObject("user", user);
        return mv;
    }

    @RequestMapping(value = "/Reset", method = RequestMethod.GET)@ResponseBody
    public ModelAndView ResetPassword(@RequestParam String senha, @RequestParam String email)
    {
    	PessoaDTO user = new PessoaDTO();
        ModelAndView mv = new ModelAndView("/account/Reset-Password");
        user.setEmail(email);
        user.setSenha(senha);
        mv.addObject("user", user);
        return mv;
    }

    //Metodo para solicitar nova senha
    @RequestMapping(value = "/NovaSenha", method = RequestMethod.POST)
    public ModelAndView SolicitaNovaSenha(PessoaDTO user) throws MalformedURLException, EmailException, NoSuchAlgorithmException
    {

        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());
        if (userResult != null)
        {
        	String senha = password.Criptografar(password.GerarPassword());
        	String url = TemplateEmail.get.forgotPassword("<a style='color:#FFF; font-style:bold; text-decoration:none;' href=http://localhost:8080/Reset?"
                                                         +"email="+user.getEmail()
                                                         +"&senha="+senha
                                                         +">clique aqui</a>");
        	userResult.setSenha(senha);
        	pessoaDAO.save(userResult);
        	System.out.println(senha);
        	email.EnviarTextoHTML(user.getEmail(), "Troca de Senha", url, "Num deu");

            ModelAndView mv = new ModelAndView("/account/Check-Email");
            mv.addObject("user", user);
            return mv;

        }

        ModelAndView mv = new ModelAndView("/account/Fail-Email");
        mv.addObject("user", user);
        return mv;
    }

    
    //Metodo para atualizar senha
    @RequestMapping(value = "/Account/Update-Password", method = RequestMethod.POST)
    public ModelAndView AtualizaSenha(PessoaDTO user) throws NoSuchAlgorithmException
    {

        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());
        if (password.Valida(user.getSenha(), userResult.getSenha(), true))
        {
            userResult.setSenha(password.Criptografar(user.getNovaSenha()));

            pessoaDAO.save(userResult);

            return platformCTL.IndexPlatform(userResult);
        }

        return EsqueciMinhaSenha();
    }

    //Metodo para atualizar senha
    @RequestMapping(value = "/AtualizaSenhaGeral", method = RequestMethod.POST)
    public ModelAndView AtualizaSenhaGeral(PessoaDTO user) throws NoSuchAlgorithmException
    {

        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());
        if (password.Valida(user.getSenha(), userResult.getSenha()))
        {
            userResult.setSenha(password.Criptografar(user.getNovaSenha()));

            pessoaDAO.save(userResult);

        }

        return platformCTL.IndexPlatform(userResult);
    }


    @RequestMapping(value = "/FazerLogin", method = RequestMethod.POST)
    public  ModelAndView FazerLogin(PessoaDTO user) throws NoSuchAlgorithmException
    {

        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());

        if (userResult == null){
            return CadastrarUser(user);
        }
        else if (password.Valida(user.getSenha(),userResult.getSenha())) {
            return platformCTL.IndexPlatform(userResult);
        }

        return Login();
    }

    
    @RequestMapping(value = "/CadastroPessoal", method = RequestMethod.POST)
    public  ModelAndView CadastroPessoal(PessoaDTO user, PessoaGeneroDTO genres) throws NoSuchAlgorithmException
    {
        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());
             	
        if (userResult == null) {

            user.setSenha(password.Criptografar(user.getSenha()));
            pessoaDAO.save(user);

            userResult = pessoaDAO.findByEmail(user.getEmail());

            return atualizaPerfilUser(userResult, genres);
        }

        return null; // fazer tela de ja cadastrado
    }

    private ModelAndView atualizaPerfilUser(PessoaDTO user, PessoaGeneroDTO genres) {
        try{

            ArrayList<PessoaGeneroDTO> listGenero = pessoaGeneroDAO.findByidPessoa(user.idPessoa);

            listGenero.forEach((f)->{
                pessoaGeneroDAO.delete(f);
            });

            String[] generos = null;
            generos = genres.getIdsGenero().split(",");
            for (String g : generos) {
                if (!g.equals("0")){
                    PessoaGeneroDTO pessoaGeneroDTO = new PessoaGeneroDTO();
                    pessoaGeneroDTO.setIdGenero(Long.parseLong(g));
                    pessoaGeneroDTO.setIdPessoa(user.getIdPessoa());
                    pessoaGeneroDAO.save(pessoaGeneroDTO);
                }
            }
        }
        catch (Exception e){

        }

        return platformCTL.IndexPlatform(user);
    }


    @RequestMapping("/AlterarMinhaSenha")
    public ModelAndView AlterarMinhaSenha()
    {
        PessoaDTO user = new PessoaDTO();
        ModelAndView mv = new ModelAndView("/platform/Alter-Password");
        mv.addObject("user", user);
        return mv;
    }

    @RequestMapping("/AtualizarMeuPerfil")
    public ModelAndView AtualizarMeuPerfil(PessoaDTO user, PessoaGeneroDTO genres)
    {
        PessoaDTO userResult = pessoaDAO.findByEmail(user.getEmail());

        userResult.setNomePessoa(user.getNomePessoa());
        pessoaDAO.save(userResult);

        return atualizaPerfilUser(userResult, genres);
    }

}
