package com.thedionisio;

import com.thedionisio.service.storage.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class TheDionisio {

	public static void main(String[] args) {
		SpringApplication.run(TheDionisio.class, args);
	}



}
