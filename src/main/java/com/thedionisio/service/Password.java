package com.thedionisio.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Password {
	        
	    public String Criptografar(String password) throws NoSuchAlgorithmException {
	        MessageDigest md = MessageDigest.getInstance("MD5");
	 
	        BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
	 
	        return String.format("%32x", hash);
	    }
	    
	    public boolean Valida(String senhaDigitada, String senhaBanco) throws NoSuchAlgorithmException
	    {
	    	if(senhaBanco.equals(Criptografar(senhaDigitada)))
	    	{
	    		return true;
	    	}
	    	return false;
	    }

	public boolean Valida(String senhaDigitada, String senhaBanco, boolean estaCriptografada) throws NoSuchAlgorithmException
	{
		if(senhaBanco.equals(senhaDigitada))
		{
			return true;
		}
		return false;
	}
	    
	    public String GerarPassword(){
	    	String senha ="";
	    	Random gerador = new Random();
	    	
	    	for(int i=0;i<24;i++)
	    	{
	    		senha += String.valueOf(gerador.nextInt(10));
	    	}
	    	
	    	
	    	return senha;
	    }
	}
	 


