package com.thedionisio.service.enums;

/**
 * Created by jonathan on 06/11/16.
 */
public interface OperacaoCrud {

    public  String  save = "save";
    public  String update = "update";
    public  String fail = "fail";
    public  String none = "nome";
    public  String delete = "delete";

}
