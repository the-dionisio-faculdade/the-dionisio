package com.thedionisio.service.enums;

/**
 * Created by jonathan on 22/10/16.
 */
public interface  Message {
    String FILIAL_ADD_SUCESS = "filial adicionada com sucesso ! :)";
    String FILIAL_ADD_FAIL = "esse cnpj já esta cadastrado ! :(";
    String FILIAL_DELETE_SUCESS = "filial deletada com sucesso ! :/";
}
