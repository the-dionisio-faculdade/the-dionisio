### 1-) Crie um branch para trabalhar.
vc pode fazer isso no site msm

###2-) Set o seu branch e comece a trabalhar nele. 

```
#!git

git checkout [nome_do_seu_branch]
```


###3-) Quando terminar de trabalhar...
faça o seu commit normalmente, pode até dar PUSH (DESDE QUE ESTAJA NO SEU, NO SEU BRANCH)

**4-) Set para o branch master e realize o pull para ficar igual a versão da cloud.** 

```
#!git

git pull
```


###5-) Set para o SEU branch novamente e faça o rebase.

```
#!git

git checkout [nome_do_seu_branch] 
git rebase master
```


###6-) Set para o branch MASTER e faça o merge com --squash.

```
#!GIT

git merge [nome_do_seu_branch] --squash
```


###7-) Faça o commit normalmente.

```
#!git

git add . 
git commit -m"UM COMENTARIO DETALHADO PLS" > git push 
```


**8-) REZE.****